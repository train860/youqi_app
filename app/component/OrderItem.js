/**
 * @author train
 */

import React, { Component } from 'react'
import {
  View,
  Text,
  Easing,
  Image,
  PixelRatio,
  Animated,
  Platform,
  StyleSheet,
  Dimensions,
  ScrollView,
  TouchableHighlight,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native'

import { px2dp, getBorderWidth, isIos, imageServer } from '../utils'

const { width, height } = Dimensions.get('window')
const borderWidth = getBorderWidth()

export class Button extends Component {
  render() {
    return (
      <TouchableOpacity
        style={[styles.button]}
        onPress={() => this.props.onPress && this.props.onPress()}
      >
        <Text style={{ fontSize: 12 }}>{this.props.children}</Text>
      </TouchableOpacity>
    )
  }
}

export default class OrderItem extends Component {
  constructor(props) {
    super(props)
  }

  renderImage() {
    const productImage = this.props.data.productImage
    const str = productImage.split('#')
    return str.map((item, index) => {
      if (index > 3 || item == '') {
        return null
      }
      return (
        <View key={index} style={styles.imageView}>
          <Image style={styles.image} source={{ uri: imageServer + item }} />
        </View>
      )
    })
  }
  renderState(state, payType,isRejected) {
    if (state == 1000 && payType != 0) {
      return <Text style={{ fontSize: 12, color: '#e23f3c' }}>待付款</Text>
    } else if (state == 1100 || (state == 1000 && payType == 0)) {
      return <Text style={{ fontSize: 12 }}>配送中</Text>
    } else if (state == 1110 || state == 1010) {
      return <Text style={{ fontSize: 12 }}>配送中</Text>
    } else if (state >= 2000) {
      if(isRejected){
        return <Text style={{ fontSize: 12 }}>已完成(拒收)</Text>
      }
      return <Text style={{ fontSize: 12 }}>已完成</Text>
    } else if (state >= -2000 && state <= -1000) {
      return <Text style={{ fontSize: 12 }}>已取消</Text>
    }
  }
  renderAction(state, payType,isRejected) {
    let canComment = false
    let canPay = false
    let canCancel = false
    if (state >= 2000 && state % 10 != 1) {
      canComment = true
    }
    if (state == 1000 && payType != 0) {
      canPay = true
    }
    if (state == 1000) {
      canCancel = true
    }
    return (
      <View style={styles.r3}>
        {canComment&&!isRejected ? (
          <Button onPress={() => this.props.onAction('Comment')}>评价</Button>
        ) : null}
        {canPay ? (
          <Button onPress={() => this.props.onAction('Payment')}>支付订单</Button>
        ) : (
          <Button onPress={() => this.props.onPress()}>查看订单</Button>
        )}

        {/**
        {canCancel ? (
          <Button onPress={() => this.props.onAction('Cancel')}>取消订单</Button>
        ) : null}
        * */}
      </View>
    )
  }
  render() {
    const { data } = this.props
    const productImage = data.productImage
    const str = productImage.split('#')
    const len = str.length - 1
    return (
      <TouchableOpacity
        activeOpacity={1.0}
        style={styles.container}
        onPress={() => this.props.onPress()}
      >
        <View style={styles.r1}>
          <View style={{ alignSelf: 'flex-start', flex: 1 }}>
            <Text style={{ color: '#999' }}>{data.createdAt}</Text>
          </View>
          <View
            style={{
              width: 120,
              alignItems: 'flex-end',
              justifyContent: 'center',
            }}
          >
            {this.renderState(data.state, data.payType,data.isRejected)}
          </View>
        </View>
        <View style={styles.r2}>
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
            {this.renderImage()}
            <Text style={{ color: '#999' }}>共{len}件</Text>
          </View>
          <View
            style={{
              width: 60,
              flex: 1,
              alignItems: 'flex-end',
              justifyContent: 'center',
            }}
          >
            <Text style={{ color: '#e23f3c' }}>¥{data.totalPrice}</Text>
          </View>
        </View>
        {this.renderAction(data.state, data.payType,data.isRejected)}
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',

    marginTop: 10,
  },
  imageView: {
    padding: 2,
    backgroundColor: 'white',
    marginRight: 8,
  },
  image: {
    width: 36,
    height: 36,
    backgroundColor: 'white',
  },
  r1: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 10,
    paddingHorizontal: 16,
  },
  r2: {
    flex: 1,
    flexDirection: 'row',
    paddingVertical: 10,
    paddingHorizontal: 16,
    backgroundColor: '#f9f9f9',
  },
  r3: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingVertical: 10,
    paddingHorizontal: 16,
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#999',
    borderRadius: isIos() ? 3 : 0,
    width: 70,
    height: 24,
    marginLeft: 8,
    borderWidth,
  },
})
