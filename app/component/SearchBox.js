import React, { Component } from 'react'
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
  TouchableOpacity,
  Dimensions,
  Keyboard,
} from 'react-native'

const { width, height } = Dimensions.get('window')
import Icon from 'react-native-vector-icons/Ionicons'
import { isIos } from '../utils'

const containerHeight = 40
const middleHeight = 20

export default class SearchBox extends Component {
  constructor(props) {
    super(props)

    this.state = {
      keyword: '',
    }
    this.placeholder = this.props.placeholder || 'Search'
    this.cancelTitle = this.props.cancelTitle || 'Cancel'
    this.focus = this.focus.bind(this)
  }
  /**
 * onSearch
 * async await
 */
  onSearch = async () => {
    if (this.props.keyboardShouldPersist === false) {
      await Keyboard.dismiss()
    }

    this.props.onSearch && (await this.props.onSearch(this.state.keyword))
  }
  onChangeText = async text => {
    await this.setState({ keyword: text })

    this.props.onChangeText &&
      (await this.props.onChangeText(this.state.keyword))
  }
  /**
     * onFocus
     * async await
     */
  onFocus = async () => {
    this.refs.input_keyword.isFocused && (await this.refs.input_keyword.focus())
    this.props.onFocus && (await this.props.onFocus(this.state.keyword))
  }

  /**
     * focus
     * async await
     */
  focus = async (text = '') => {
    await this.setState({ keyword: text })
    await this.refs.input_keyword.focus()
  }

  /**
     * onDelete
     * async await
     */
  onDelete = async () => {
    await this.setState({ keyword: '' })
    this.props.onDelete && (await this.props.onDelete())
  }

  /**
     * onCancel
     * async await
     */
  onCancel = async () => {
    this.props.onCancel && (await this.props.onCancel())
  }

  render() {
    return (
      <View
        style={{
          height: isIos() ? 42 : 64,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <View
          style={[
            styles.container,
            this.props.backgroundColor && {
              backgroundColor: this.props.backgroundColor,
            },
          ]}
        >
          <TextInput
            style={[
              styles.input,
              this.props.inputHeight && { height: this.props.inputHeight },
            ]}
            ref="input_keyword"
            value={this.state.keyword}
            onChangeText={this.onChangeText}
            onSubmitEditing={this.onSearch}
            placeholder={this.placeholder}
            placeholderTextColor={
              this.props.placeholderTextColor || styles.placeholderColor
            }
            onFocus={this.onFocus}
            underlineColorAndroid="transparent"
            autoCorrect={false}
          />
          <TouchableWithoutFeedback onPress={this.onFocus}>
            <Image
              source={require('./img/search.png')}
              style={[
                styles.iconSearch,
                styles.iconSearchDefault,

                {
                  left: 5,
                },
              ]}
            />
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={this.onDelete}>
            <Image
              source={require('./img/delete.png')}
              style={[styles.iconDelete, styles.iconDeleteDefault]}
            />
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={this.onCancel}>
            <View style={[styles.cancelButton, { left: 10 }]}>
              <Text
                style={[
                  styles.cancelButtonText,
                  this.props.titleCancelColor && {
                    color: this.props.titleCancelColor,
                  },
                ]}
              >
                {this.cancelTitle}
              </Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    )
  }
}
SearchBox.defaultProps = {
  keyboardShouldPersist: false,
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'grey',
    height: containerHeight,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: 5,
  },
  input: {
    flex: 1,
    height: containerHeight,
    paddingLeft: 20,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 20,
    borderColor: '#444',
    backgroundColor: '#f7f7f7',
    borderRadius: 3,
    fontSize: 13,
  },
  // placeholderColor: 'grey',
  iconSearch: {
    flex: 1,
    position: 'absolute',
    top: middleHeight - 7,
    height: 14,
    width: 14,
  },
  iconSearchDefault: {
    tintColor: 'grey',
  },
  iconDelete: {
    position: 'absolute',
    right: 50,
    top: middleHeight - 7,
    height: 14,
    width: 14,
  },
  iconDeleteDefault: {
    tintColor: 'grey',
  },
  cancelButton: {
    justifyContent: 'center',
    alignItems: 'flex-start',
    backgroundColor: 'transparent',
    width: 38,
    height: 50,
  },
  cancelButtonText: {
    fontSize: 14,
    color: '#999',
  },
})
