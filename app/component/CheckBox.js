import React, { Component } from 'react'
import {
  Text,
  View,
  Image,
  StyleSheet,
  Platform,
  ScrollView,
  Dimensions,
  AlertIOS,
  Animated,
  RefreshControl,
  TouchableOpacity,
  TouchableWithoutFeedback,
  FlatList,
} from 'react-native'

import { NavigationActions, px2dp, isIos } from '../utils'
import Icon from 'react-native-vector-icons/Ionicons'

const InputHeight = px2dp(28)

export default class CheckBox extends Component {
  onPress = async () => {
    this.props.onPress && (await this.props.onPress())
  }
  render() {
    const { checked, width, height, color } = this.props

    return (
      <View style={(width && { width }, height && { height })}>
        <TouchableOpacity onPress={() => this.onPress()}>
          {checked ? (
            <Icon
              name="ios-checkmark-circle"
              size={width}
              style={color && { color }}
            />
          ) : (
            <Icon
              name="ios-radio-button-off-outline"
              size={width}
              style={{ color: '#333' }}
            />
          )}
        </TouchableOpacity>
      </View>
    )
  }
}
CheckBox.defaultProps = {
  height: 24,
  width: 24,
  checked: false,
}
const styles = StyleSheet.create({})
