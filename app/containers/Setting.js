import React, { Component } from 'react'
import {
  Text,
  View,
  Image,
  StyleSheet,
  ScrollView,
  Dimensions,
  Platform,
  PixelRatio,
  AlertIOS,
  TouchableOpacity,
  TouchableHighlight,
  TouchableNativeFeedback,
  TouchableWithoutFeedback,
  RefreshControl,
  DeviceEventEmitter,
  Alert,
} from 'react-native'
import Immutable from 'immutable'
import Icon from 'react-native-vector-icons/Ionicons'
import Item from '../component/Item'
import RootView from '../component/RootView'
import Base from './Base'
import {
  NavigationActions,
  px2dp,
  createAction,
  removeAsyncStorage,
} from '../utils'
import * as userService from '../services/user'
import LocalImg from '../images'

const { width, height } = Dimensions.get('window')

const borderWidth = (Platform.OS === 'ios' ? 1.0 : 1.0) / PixelRatio.get()

class Setting extends Base {
  constructor(props) {
    super(props)
    this.state = {
      fetching: false,
    }
    this.config = [
      {
        icon: 'ios-log-out-outline',
        name: '注销登录',
        first: true,
        onPress: () => {
          this.logout()
        },
      },
    ]
  }
  static navigationOptions = {
    title: '设置',
    headerBackTitle: null,
    headerTitleStyle: {
      alignSelf: 'center',
    },
    headerStyle: {
      backgroundColor: '#fff',
      borderBottomWidth: 0,
      shadowColor: 'transparent',
      elevation: 0,
    },
    headerRight: <View />,
  }
  logout = () => {
    this.setState({ fetching: true })
    removeAsyncStorage('APP_TOKEN').then(data => {
      if (data) {
        // 注销成功
        DeviceEventEmitter.emit('account.reload')
        setTimeout(() => {
          this.setState({ fetching: false })
          this.goBack()
        }, 300)
      } else {
        this.setState({ fetching: false })
        Alert.alert('注销失败')
      }
    })
  }

  renderListItem = () =>
    this.config.map((item, i) => <Item key={i} {...item} />)

  componentDidMount() {}
  render() {
    const { fetching } = this.state
    return (
      <RootView fetching={fetching} style={styles.container}>
        <ScrollView>{this.renderListItem()}</ScrollView>
      </RootView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  userHead: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 20,
    backgroundColor: '#0398ff',
  },
  numbers: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    height: 74,
  },
  numItem: {
    flex: 1,
    height: 74,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default Setting
