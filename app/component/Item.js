/**
 * @author Lei
 * @repo https://github.com/stoneWeb/elm-react-native
 */

import React, { Component, PropTypes } from 'react'
import {
  Text,
  View,
  Image,
  StyleSheet,
  Dimensions,
  Platform,
  PixelRatio,
  TouchableHighlight,
  AlertIOS,
  TouchableNativeFeedback,
} from 'react-native'
import { px2dp } from '../utils'
import Button from './Button'
import Ionicons from 'react-native-vector-icons/Ionicons'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

const { width, height } = Dimensions.get('window')
const borderWidth = (Platform.OS === 'ios' ? 1.0 : 1.0) / PixelRatio.get()

const itemHeight = px2dp(45)

const Font = {
  Ionicons,
  FontAwesome,
}
class ItemButton extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <Button
        style={{ marginTop: this.props.first ? 10 : 0 }}
        onPress={this.props.onPress}
      >
        <View style={styles.button}>
          <Text style={{ color: this.props.color || '#f00' }}>
            {this.props.name}
          </Text>
        </View>
      </Button>
    )
  }
}

export default class Item extends Component {
  constructor(props) {
    super(props)
  }

  _render() {
    let {
      icon,
      iconSize,
      name,
      subName,
      color,
      first,
      avatar,
      disable,
      font,
    } = this.props
    font = font || 'Ionicons'
    const Icon = Font[font]
    return (
      <View style={styles.listItem}>
        {icon ? (
          <Icon
            name={icon}
            size={px2dp(iconSize || 20)}
            style={{ width: 22, marginRight: 5, textAlign: 'center' }}
            color={color || '#4da6f0'}
          />
        ) : null}
        <View
          style={[
            styles.listInfo,
            { borderTopWidth: !first ? borderWidth : 0 },
          ]}
        >
          <View style={{ flex: 1 }}>
            <Text>{name}</Text>
          </View>
          <View style={styles.listInfoRight}>
            {subName ? (
              <Text style={{ color: '#aaa', fontSize: 12 }}>{subName}</Text>
            ) : null}
            {avatar ? (
              <Image
                source={avatar}
                style={{
                  width: 36,
                  height: 36,
                  resizeMode: 'cover',
                  overflow: 'hidden',
                  borderRadius: 18,
                }}
              />
            ) : null}
            {disable ? null : (
              <Font.Ionicons
                style={{ marginLeft: 10 }}
                name="ios-arrow-forward-outline"
                size={px2dp(18)}
                color="#bbb"
              />
            )}
          </View>
        </View>
      </View>
    )
  }
  render() {
    let { onPress, first, disable } = this.props
    onPress = onPress || (() => {})
    return disable ? (
      this._render()
    ) : (
      <Button style={{ marginTop: first ? 10 : 0 }} onPress={onPress}>
        {this._render()}
      </Button>
    )
  }
}
Item.Button = ItemButton
const styles = StyleSheet.create({
  listItem: {
    height: itemHeight,
    paddingLeft: 16,
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    height: itemHeight,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  listInfo: {
    height: itemHeight,
    flex: 1,
    paddingRight: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderTopColor: '#f5f5f5',
  },
  listInfoRight: {
    flexDirection: 'row',
    alignItems: 'center',
  },
})
