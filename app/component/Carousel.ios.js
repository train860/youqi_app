import React, { Component } from 'react'
import { View, Image, Dimensions, TouchableWithoutFeedback } from 'react-native'
import Swiper from 'react-native-swiper'
import { imageServer } from '../utils'

const { width, height } = Dimensions.get('window')

export default class Carousel extends Component {
  render() {
    const { data, onItemPress } = this.props
    return (
      <Swiper
        loop
        height={205}
        borderRadius={0}
        backgroundColor="#f6f6f6"
        paginationStyle={{ bottom: 5 }}
        dotStyle={{
          backgroundColor: 'rgba(255,255,255,.5)',
          width: 12,
          height: 2,
          borderRadius: 0,
        }}
        activeDotStyle={{
          backgroundColor: 'rgba(255,255,255,1)',
          width: 12,
          height: 2,
          borderRadius: 0,
        }}
      >
        {data.map((item, i) => (
          <TouchableWithoutFeedback
            key={i}
            onPress={() => onItemPress && onItemPress(item)}
          >
            <Image
              source={{ uri: imageServer + item.image }}
              style={{ height: 205, width, resizeMode: 'cover' }}
            />
          </TouchableWithoutFeedback>
        ))}
      </Swiper>
    )
  }
}
