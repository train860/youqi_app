import React, { Component } from 'react'
import {
  Text,
  View,
  Alert,
  BackAndroid,
  StatusBar,
  ScrollView,
  StyleSheet,
  AlertIOS,
  RefreshControl,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TouchableNativeFeedback,
  TouchableHighlight,
  Image,
  TextInput,
  Platform,
  PixelRatio,
  Dimensions,
  ActivityIndicator,
  Animated,
  Modal,
  Linking,
  FlatList,
  NativeModules,
  NativeAppEventEmitter,
  DeviceEventEmitter,
} from 'react-native'
import { connect } from 'react-redux'
import LocalImg from '../images'
import Icon from 'react-native-vector-icons/Ionicons'
import AMapLocation from 'react-native-amap-location'

import Carousel from '../component/Carousel'

import Loader from '../component/Loader'
import MessageModal from '../component/MessageModal'
import SearchView from '../component/SearchView'
import GoodsItem from '../component/GoodsItem'
import RootView from '../component/RootView'
import Base from './Base'
import Search from './Search'
import {
  imageServer,
  NavigationActions,
  px2dp,
  getBorderWidth,
  getVersion,
  getAsyncStorage,
  setAsyncStorage,
} from '../utils'
import { toast } from '../utils/toast'
import * as appService from '../services/app'
import * as productService from '../services/product'

const isIOS = Platform.OS == 'ios'
const { width, height } = Dimensions.get('window')

const borderWidth = getBorderWidth()

const headH = px2dp(isIOS ? 120 : 100)
const InputHeight = px2dp(28)
class Home extends Base {
  constructor(props) {
    super(props)
    this.state = {
      carousel: [],
      category: [],
      area: [],
      channel: [],
      recommend: [],
      locationFromVillage: false,
      location: '定位中',
      scrollY: new Animated.Value(0),
      searchView: new Animated.Value(0),
      modalVisible: false,
      modalType: '',
      listLoading: false,
      isRefreshing: false,
      requestTimes: 0,
      notConnected: false,
      barStyle: 'light-content',
    }
    this.popupDialog = null

    this.SEARCH_BOX_Y = px2dp(isIOS ? 48 : 43)
    this.SEARCH_FIX_Y = headH - px2dp(isIOS ? 64 : 44)
    this.SEARCH_KEY_P = px2dp(58)
    this.SEARCH_DIFF_Y = this.SEARCH_FIX_Y - this.SEARCH_BOX_Y
    this.SEARCH_FIX_DIFF_Y = headH - this.SEARCH_FIX_Y - headH

    this.subscription = null
    this.navListener = null
  }
  static navigationOptions = {
    title: null,
    headerBackTitle: null,
    header: null,
    tabBarLabel: '首页',
    tabBarIcon: ({ focused, tintColor }) => {
      if (focused) {
        return <Icon name="ios-home" style={{ color: tintColor }} size={30} />
      }
      return (
        <Icon name="ios-home-outline" style={{ color: 'gray' }} size={30} />
      )
    },
  }
  linkTo = url => {
    Linking.canOpenURL(url)
      .then(supported => {
        if (!supported) {
        } else {
          return Linking.openURL(url)
        }
      })
      .catch(err => console.error('An error occurred', err))
  }
  loadData() {
    const { location, city, district } = this.state
    if (location && city && district) {
      this.fetchingData(location, city, district)
    } else {
      getAsyncStorage('LOCATION')
        .then(data => {
          const d = data.split('#')
          this.fetchingData(d[0], d[1], d[2])
        })
        .catch(e => {})
    }
  }

  fetchingData(name, city, district, reload = false) {
    const type = isIOS ? 1 : 0
    this.setState({ fetching: true })

    if (!name) {
      name = this.state.location
    }

    if (!city) {
      city = this.state.city
    }

    if (!district) {
      district = this.state.district
    }
    const str = `${name},${city},${district}`
    const { locationFromVillage, requestTimes } = this.state

    appService
      .home({ type, name, city, district })
      .then(data => {
        StatusBar.setBarStyle('light-content')
        setAsyncStorage('LOCATION', `${name}#${city}#${district}`)
        setAsyncStorage('CONFIG', JSON.stringify(data.data.config))
        // 店铺id放入缓存
        setAsyncStorage('STOREID', data.data.storeId).then(b => {
          if (b) {
            this.setState({
              ...data.data,
              location: name,
              fetching: false,
              refreshing: false,
              notConnected: false,
              city,
              district,
              requestTimes: requestTimes + 1,
            })
            // 如果有新版本，需要提示
            if (!reload && data.data.version && !locationFromVillage) {
              const onlineVersion = data.data.version.version.replace('.', '')
              if (onlineVersion > getVersion().replace('.', '')) {
                Alert.alert('新版本提醒', data.data.version.content, [
                  {
                    text: '取消',
                    style: 'cancel',
                  },
                  { text: '去更新', onPress: () => this.linkTo(data.data.version.link) },
                ])
              }
            }
          } else {
            this.setState({
              notConnected: false,
              fetching: false,
              refreshing: false,
              requestTimes: requestTimes + 1,
            })
          }
        })
      })
      .catch(e => {
        if (e.code == 10000) {
          if (locationFromVillage) {
            this.setState({
              fetching: false,
              refreshing: false,
              notConnected: false,
              modalVisible: true,
              modalType: 'location',
              requestTimes: requestTimes + 1,
            })
            return
          }
          this.setState({
            fetching: false,
            refreshing: false,
            notConnected: false,
            modalVisible: true,
            modalType: 'location',
            location: name,
            city,
            district,
            requestTimes: requestTimes + 1,
          })
        } else if (e.code == 300) {
          this.setState({
            notConnected: true,
            fetching: false,
            refreshing: false,
            requestTimes: requestTimes + 1,
          })
        } else {
          this.setState({
            notConnected: false,
            fetching: false,
            refreshing: false,
            requestTimes: requestTimes + 1,
          })
          toast(e.msg)
        }
      })
  }

  onLocationEvent(data) {
    if (data) {
      this.fetchingData(data.name, data.city, data.district, true)
    }
  }

  componentWillUnmount() {
    this.subscription.remove()
    this.navListener.remove()
  }
  componentDidMount() {
    this.subscription = DeviceEventEmitter.addListener(
      'location.reload',
      this.onLocationEvent.bind(this)
    )
    this.navListener = this.props.navigation.addListener('didFocus', () => {
      const y = parseInt(JSON.stringify(this.state.scrollY))
      if (y < this.SEARCH_FIX_Y) {
        StatusBar.setBarStyle('light-content')
      }

      // isAndroid && StatusBar.setBackgroundColor('#6a51ae');
    })
    this.loadData()
  }
  _renderHeader() {
    const searchY = this.state.scrollY.interpolate({
      inputRange: [0, this.SEARCH_BOX_Y, this.SEARCH_FIX_Y, this.SEARCH_FIX_Y],
      outputRange: [0, 0, this.SEARCH_DIFF_Y, this.SEARCH_DIFF_Y],
    })
    const lbsOpaticy = this.state.scrollY.interpolate({
      inputRange: [0, this.SEARCH_BOX_Y],
      outputRange: [1, 0],
    })
    const keyOpaticy = this.state.scrollY.interpolate({
      inputRange: [0, this.SEARCH_BOX_Y, this.SEARCH_KEY_P],
      outputRange: [1, 1, 0],
    })
    return (
      <View
        style={[
          styles.header,
          {
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            zIndex: 99,
            backgroundColor: 'transparent',
            paddingTop: px2dp(isIOS ? 25 : 16),
          },
        ]}
      >
        <Animated.View style={[styles.lbsWeather, { opacity: lbsOpaticy }]}>
          <TouchableWithoutFeedback onPress={() => this.goto('Village')}>
            <View style={styles.lbs}>
              <Icon name="ios-pin" size={px2dp(16)} color="#fff" />
              <Text
                style={{
                  color: '#fff',
                  paddingHorizontal: 5,
                }}
              >
                {this.state.location}
              </Text>
              <Icon name="md-arrow-dropdown" size={px2dp(16)} color="#fff" />
            </View>
          </TouchableWithoutFeedback>
        </Animated.View>
        <Animated.View
          style={{
            marginTop: 5,
            transform: [
              {
                translateY: searchY,
              },
            ],
          }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              this.openSearch()
            }}
          >
            {
              <View
                style={[
                  styles.searchBtn,
                  { backgroundColor: 'rgba(255,255,255,0.8)' },
                ]}
              >
                <Icon name="ios-search-outline" size={20} color="#666" />
                <Text style={{ color: '#666', marginLeft: 8 }}>{'输入商品名称'}</Text>
              </View>
            }
          </TouchableWithoutFeedback>
        </Animated.View>
      </View>
    )
  }
  _renderFixHeader() {
    const showY = this.state.scrollY.interpolate({
      inputRange: [0, this.SEARCH_BOX_Y, this.SEARCH_FIX_Y, this.SEARCH_FIX_Y],
      outputRange: [-9999, -9999, 0, 0],
    })

    return (
      <Animated.View
        style={[
          styles.header,
          {
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            elevation: 2,
            height: px2dp(isIOS ? 75 : 56),
            paddingTop: px2dp(isIOS ? 32 : 16),
            transform: [{ translateY: showY }],
          },
        ]}
      >
        <TouchableWithoutFeedback
          onPress={() => {
            this.openSearch()
          }}
        >
          <View style={[styles.searchBtn, { backgroundColor: '#f0f0f0' }]}>
            <Icon name="ios-search-outline" size={20} color="#666" />
            <Text style={{ color: '#666', marginLeft: 5 }}>{'输入商品名称'}</Text>
          </View>
        </TouchableWithoutFeedback>
      </Animated.View>
    )
  }
  openSearch() {
    this.setState({
      barStyle: 'default',
      modalType: 'search',
      modalVisible: true,
    })
    const y = parseInt(JSON.stringify(this.state.scrollY))
    if (y < this.SEARCH_FIX_Y) {
      StatusBar.setBarStyle('default')
    }
  }
  closeModal() {
    this.setState({
      modalType: '',
      modalVisible: false,
    })
    const y = parseInt(JSON.stringify(this.state.scrollY))
    if (y < this.SEARCH_FIX_Y) {
      StatusBar.setBarStyle('light-content')
    }
  }
  search(keywords) {
    const { navigation } = this.props
    const navigateAction = NavigationActions.navigate({
      routeName: 'GoodsList',
      params: { keywords },
    })
    this.closeModal()
    navigation.dispatch(navigateAction)
  }

  changeLocation(location) {
    this.setState({ location })
  }
  _renderAds() {
    const { carousel } = this.state
    return (
      <View
        style={{
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
          // paddingLeft: 16,
          // paddingRight: 16,
          // paddingTop: 10,
        }}
      >
        <Carousel
          data={carousel}
          onItemPress={item => {
            const { link } = item

            if (link.indexOf('youqi://') == 0) {
              this.goto('Goods', { goodsId: link.split('://')[1], isSku: 1 })
            } else if (
              link.indexOf('http://') == 0 ||
              link.indexOf('https://') == 0
            ) {
              this.goto('Browser', { uri: link })
            }else{
              //直接搜索商品
              this.goto('GoodsList',{keywords:link})
            }
          }}
        />
      </View>
    )
  }
  _renderTypes() {
    const w = width / 4,
      h = w * 0.6 + 20
    const renderSwipeView = (types, n) => (
      <View style={styles.typesView}>
        {types.map((item, i) => {
          const render = (
            <View style={[{ width: w, height: h }, styles.typesItem]}>
              <Image
                source={{ uri: imageServer + item.icon }}
                style={{ width: w * 0.4, height: w * 0.4 }}
              />
              <Text style={{ marginTop: 5, fontSize: 12 }}>{item.name}</Text>
            </View>
          )
          return (
            <TouchableWithoutFeedback
              activeOpacity={0.8}
              style={{ width: w, height: h }}
              key={String(i)}
              onPress={() => {
                this.goto('GoodsList', { caid: item.id, cbid: 0 })
              }}
            >
              {render}
            </TouchableWithoutFeedback>
          )
        })}
      </View>
    )
    return <View>{renderSwipeView(this.state.category, 8)}</View>
  }
  _renderHot() {
    const w = width / 3
    const { area } = this.state

    if (area.length == 0) {
      return null
    }
    const renderRight = () =>
      area.map((n, i) => {
        if (i == 0) {
          return null
        }
        const styl = {
          1: {
            borderBottomWidth: 1,
            borderBottomColor: '#f9f9f9',
            borderRightWidth: 1,
            borderRightColor: '#f9f9f9',
          },
          2: {
            borderBottomWidth: 1,
            borderBottomColor: '#f9f9f9',
          },
          3: {
            borderRightWidth: 1,
            borderRightColor: '#f9f9f9',
          },
          4: {},
        }
        const _render = i => (
          <View style={styles.recomWrap}>
            <Image
              source={{ uri: imageServer + n.image }}
              style={{ width: w - 2, height: w - 2, resizeMode: 'contain' }}
            />
          </View>
        )

        return isIOS ? (
          <View
            key={String(i)}
            style={[styles.recomItem, styl[i], { backgroundColor: '#f5f5f5' }]}
          >
            <TouchableHighlight
              onPress={() => {
                const { link } = n
                if (link.indexOf('youqi://') == 0) {
                  this.goto('Goods', {
                    goodsId: link.split('://')[1],
                    isSku: 1,
                  })
                } else if (
                  link.indexOf('http://') == 0 ||
                  link.indexOf('https://') == 0
                ) {
                  this.goto('Browser', { uri: link })
                }
              }}
              style={{ flex: 1 }}
            >
              {_render(i)}
            </TouchableHighlight>
          </View>
        ) : (
          <View key={String(i)} style={[styles.recomItem, styl[i]]}>
            <TouchableNativeFeedback
              onPress={() => {
                const { link } = n
                if (link.indexOf('youqi://') == 0) {
                  this.goto('Goods', {
                    goodsId: link.split('://')[1],
                    isSku: 1,
                  })
                } else if (
                  link.indexOf('http://') == 0 ||
                  link.indexOf('https://') == 0
                ) {
                  this.goto('Browser', { uri: link })
                }
              }}
              style={{ flex: 1, height: 70 }}
            >
              {_render(i)}
            </TouchableNativeFeedback>
          </View>
        )
      })

    return (
      <View style={{ flexDirection: 'row' }}>
        <View
          style={{
            width: w,
            backgroundColor: 'white',
            alignItems: 'center',
            justifyContent: 'center',
            borderRightWidth: borderWidth,
            borderRightColor: '#f9f9f9',
            overflow: 'hidden',
            marginTop: 10,
          }}
        >
          <TouchableHighlight
            onPress={() => {
              const { link } = area[0]
              if (link.indexOf('youqi://') == 0) {
                this.goto('Goods', { goodsId: link.split('://')[1], isSku: 1 })
              } else if (
                link.indexOf('http://') == 0 ||
                link.indexOf('https://') == 0
              ) {
                this.goto('Browser', { uri: link })
              }
            }}
          >
            <Image
              source={{ uri: imageServer + area[0].image }}
              style={{ width: w - 2, height: 140, resizeMode: 'contain' }}
            />
          </TouchableHighlight>
        </View>
        <View style={[styles.recom, { flex: 1 }]}>{renderRight()}</View>
      </View>
    )
  }
  _renderLtime() {
    const { channel } = this.state

    return (
      <View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Text style={{ fontSize: 14, fontWeight: 'bold' }}>特色推荐</Text>
          </View>

          <TouchableOpacity
            onPress={() => this.goto('GoodsList', { keywords: '特色推荐' })}
          >
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text
                style={{ fontSize: px2dp(13), color: '#aaa', marginRight: 3 }}
              >
                更多
              </Text>
              <Icon
                name="ios-arrow-forward-outline"
                size={px2dp(13)}
                color="#bbb"
              />
            </View>
          </TouchableOpacity>
        </View>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          horizontal
          style={styles.lTimeScrollView}
        >
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              paddingTop: 15,
            }}
          >
            {channel.map((item, i) => {
              if (!item || !item.imageSource) {
                return null
              }
              let price = item.price
              if (!price) {
                price =item.salePrice
              }

              const layout = (
                <View style={styles.lTimeList}>
                  <Image
                    source={{
                      uri: imageServer + item.imageSource || '',
                    }}
                    style={{
                      height: px2dp(85),
                      width: px2dp(85),
                      resizeMode: 'cover',
                    }}
                  />
                  <Text
                    numberOfLines={2}
                    style={{
                      color: '#333',
                      marginVertical: 5,
                      width: px2dp(85),
                    }}
                  >
                    {item.name}
                  </Text>
                  <View style={{ flexDirection: 'row',alignItems: 'center' }}>
                    <Text
                      style={{
                        textDecorationLine:'line-through',
                        color: '#999',
                        marginBottom: 5,
                      }}
                    >
                      ￥{item.retailPrice}
                    </Text>
                  </View>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        color: '#e23f3c',
                      }}
                    >
                      ￥{price}
                    </Text>
                  </View>
                </View>
              )
              return (
                <TouchableOpacity
                  activeOpacity={0.95}
                  key={String(i)}
                  style={{ marginRight: 10 }}
                  onPress={() =>
                    this.goto('Goods', { goodsId: item.id, isSku: 1 })}
                >
                  {layout}
                </TouchableOpacity>
              )
            })}
          </View>
        </ScrollView>
      </View>
    )
  }

  _renderBZ() {
    const cellWH = width / 3
    _renderItem = ({ item, index }) => {
      let borderRightWidth = borderWidth
      if ((index + 1) % 3 == 0) {
        borderRightWidth = 0
      }

      return (
        <GoodsItem
          key={String(index)}
          type={0}
          item={item}
          width={cellWH}
          borderRightWidth={borderRightWidth}
          onPress={() => this.goto('Goods', { goodsId: item.id, isSku: 1 })}
        />
      )
    }
    return (
      <FlatList
        keyExtractor={(item, index) => index}
        numColumns={3}
        horizontal={false}
        data={this.state.recommend}
        renderItem={_renderItem}
      />
    )
  }
  _onRefresh() {
    this.setState({ refreshing: true })
    setTimeout(() => {
      this.fetchingData(null, null, null, true)
    }, 1000)

    // this.loadLocation()
  }
  onEndReached() {
    const { pages, recommend, fetching } = this.state
    if (fetching) {
      return
    }
    let current = this.state.current
    if (!current) {
      current = 1
    }
    const pageNum = current + 1
    if (pages && pageNum > pages) {
      return
    }
    this.setState({ fetching: true })

    productService
      .recommend({ current: pageNum })
      .then(data => {
        if (data.current == current || data.records.size == 0) {
          this.setState({ fetching: false, pages: 1 })
          return
        }

        const list = recommend.concat(data.records)
        this.setState({
          fetching: false,
          recommend: list,
          pages: data.pages,
          current: data.current,
        })
      })
      .catch(e => {
        this.checkError(e)
      })
  }
  handleScroll(e) {
    const y = e.nativeEvent.contentOffset.y
    const height = e.nativeEvent.layoutMeasurement.height
    const contentHeight = e.nativeEvent.contentSize.height
    if (y >= this.SEARCH_FIX_Y) {
      StatusBar.setBarStyle('default')
    } else {
      StatusBar.setBarStyle('light-content')
    }
    if (y + height >= contentHeight - 20) {
      this.onEndReached()
    }
  }
  renderModal() {
    const { modalType, modalVisible, locationFromVillage } = this.state
    if (modalType == 'search') {
      return (
        <Search
          onCancel={() => this.closeModal()}
          onSearch={keywords => this.search(keywords)}
        />
      )
    }
  }
  render() {
    const {
      fetching,
      modalType,
      modalVisible,
      carousel,
      requestTimes,
      notConnected,
    } = this.state

    if (carousel.length == 0 && !modalVisible && requestTimes == 0) {
      return <Loader />
    }

    return (
      <RootView
        onReconnect={() => this.loadData()}
        notConnected={notConnected}
        fetching={fetching}
        nobar
        style={styles.container}
      >
        <ScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={styles.scrollView}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
            { listener: e => this.handleScroll(e) }
          )}
          scrollEventThrottle={16}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing || false}
              onRefresh={this._onRefresh.bind(this)}
              colors={['#ddd', '#0398ff']}
              progressBackgroundColor="#ffffff"
            />
          }
        >
          {this._renderHeader()}

          {this._renderAds()}

          <View style={{ backgroundColor: '#fff', paddingVertical: 10 }}>
            {this._renderTypes()}
          </View>

          {this._renderHot()}

          <View style={styles.card}>{this._renderLtime()}</View>

          <View style={styles.business}>
            <View
              style={{
                paddingHorizontal: 16,
                paddingVertical: 10,
                borderColor: '#e5e5e5',
                borderBottomWidth: borderWidth,
              }}
            >
              <Text style={{ fontWeight: 'bold' }}>{'精品推荐'}</Text>
            </View>
            {this._renderBZ()}
          </View>
        </ScrollView>
        {this._renderFixHeader()}
        <Modal
          animationType={modalType == 'search' ? 'slide' : 'fade'}
          transparent
          onRequestClose={() => {}}
          visible={this.state.modalVisible}
        >
          {this.renderModal()}
        </Modal>
      </RootView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    backgroundColor: '#fff',
    height: headH,
    paddingTop: px2dp(isIOS ? 30 : 10),
    paddingHorizontal: 16,
  },
  typesView: {
    paddingBottom: 10,
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  typesItem: {
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  lbsWeather: {
    height: InputHeight,
    overflow: 'hidden',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  placeholder: {
    height: InputHeight,
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    borderRadius: px2dp(14),
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  lbs: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  weather: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textInput: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    height: InputHeight,
    borderRadius: px2dp(14),
    backgroundColor: '#fff',
  },
  searchHeadBox: {
    height: InputHeight,
    flexDirection: 'row',
    alignItems: 'center',
  },
  searchBtn: {
    borderRadius: 3,
    height: InputHeight,
    flexDirection: 'row',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  keywords: {
    marginTop: px2dp(14),
    flexDirection: 'row',
  },
  scrollView: {},
  recom: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    marginTop: 10,
    flexWrap: 'wrap',
  },
  card: {
    backgroundColor: '#fff',
    marginTop: 10,
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  business: {
    backgroundColor: '#fff',
    marginTop: 10,
  },
  time: {
    paddingHorizontal: 3,
    backgroundColor: '#333',
    color: '#fff',
    marginHorizontal: 3,
  },
  recomItem: {
    width: width / 3,
    height: 70,
    backgroundColor: '#fff',
    alignItems: 'center',
    flexDirection: 'row',
  },
  recomWrap: {
    flex: 1,
    height: 70,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    overflow: 'hidden',
  },
  lTimeScrollView: {},
  lTimeList: {
    backgroundColor: '#fff',
    alignItems: 'center',
  },

  fixSearch: {
    backgroundColor: '#0398ff',
    height: isIOS ? 64 : 42,
    paddingTop: isIOS ? 20 : 0,
    paddingHorizontal: 16,
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
  },
})

export default Home
