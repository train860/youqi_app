/**
 * @author Lei
 * @repo https://github.com/stoneWeb/elm-react-native
 */

import React, { Component } from 'react'
import {
  Text,
  View,
  Animated,
  Platform,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  PixelRatio,
} from 'react-native'

const { width, height } = Dimensions.get('window')
import Icon from 'react-native-vector-icons/Ionicons'
import { px2dp } from '../utils'

const borderWidth = (Platform.OS === 'ios' ? 1.0 : 1.0) / PixelRatio.get()
export default class SearchView extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <ScrollView style={styles.scrollView}>
        <View style={styles.head}>
          <Text style={{ fontSize: px2dp(13), color: '#333' }}>{'历史搜索'}</Text>
          <TouchableOpacity
            onPress={() => this.props.onTrash && this.props.onTrash()}
          >
            <Icon name={'ios-trash'} size={18} color="#333" />
          </TouchableOpacity>
        </View>
        <View style={styles.queryList}>
          {this.props.data.map((item, i) => (
            <View key={i} style={{ marginRight: 12, marginBottom: 12 }}>
              <TouchableOpacity
                onPress={() =>
                  this.props.onSearch && this.props.onSearch(item.value)}
              >
                <Text style={styles.queryItem}>
                  {item.value.replace(/(^\s*)|(\s*$)/g, '')}
                </Text>
              </TouchableOpacity>
            </View>
          ))}
        </View>
        {/**
        <View style={styles.head}>
          <Text style={{ fontSize: px2dp(13), color: '#333' }}>{'热门搜索'}</Text>
        </View>
        <View style={styles.queryList}>
          {['贡茶', '大排档', '第一大排档', '果麦', '星巴克', '周黑鸭'].map((item, i) => (
            <View key={i} style={{ marginRight: 12, marginBottom: 12 }}>
              <TouchableOpacity>
                <Text style={styles.queryItem}>{item}</Text>
              </TouchableOpacity>
            </View>
          ))}
        </View>
        * */}
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  wrap: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: Platform.OS === 'ios' ? 64 : 42,
    backgroundColor: '#eee',
  },
  head: {
    paddingHorizontal: 16,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  queryList: {
    paddingHorizontal: 16,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  queryItem: {
    flex: 1,
    color: '#666',
    fontSize: 12,
    borderWidth,
    borderColor: '#bbb',
    paddingHorizontal: 8,
    paddingVertical: 3,
    borderRadius: 4,
  },
  scrollView: {
    backgroundColor: '#f9f9f9',
  },
})
