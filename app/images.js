export default {
  logo: require('./images/logo.png'),
  alipay: require('./images/alipay.png'),
  wechat: require('./images/wechat.png'),
  cash: require('./images/cash.png'),
  avatar: require('./images/avatar.png'),
}
