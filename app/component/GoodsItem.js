import React, { Component } from 'react'
import {
  View,
  Platform,
  Text,
  StyleSheet,
  PixelRatio,
  TouchableHighlight,
  TouchableOpacity,
  Image,
  TouchableWithoutFeedback,
} from 'react-native'

import Icon from 'react-native-vector-icons/Ionicons'

import { imageServer } from '../utils'

const borderWidth = (Platform.OS === 'ios' ? 1.0 : 1.0) / PixelRatio.get()

export default class GoodsItem extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    // type=0 为首页
    const {
      type,
      item,
      width,
      borderRightWidth,
      top,
      onPress,
      onCartPress,
    } = this.props
    let price = item.price
    if (!price) {
      price =
        item.salePrice > 0? item.salePrice: item.retailPrice
    }
    return (
      <View style={[styles.innerViewStyle, { width, marginTop: top }]}>
        <View style={[styles.item, { borderRightWidth }]}>
          <TouchableWithoutFeedback onPress={() => onPress()}>
            <View
              style={{

                height: type == 0 ? width : width-20,

                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Image
                source={{
                  uri:
                    imageServer +
                    (item.imageSource ? item.imageSource : item.image_source),
                }}
                style={{
                  backgroundColor: '#f9f9f9',
                  width: width-20 ,
                  height: width-20 ,
                }}
              />
            </View>
          </TouchableWithoutFeedback>
          <View style={[styles.title, { height: type == 0 ? 18 : 36 }]}>
            <Text numberOfLines={type == 0 ? 1 : 2} style={{ flex: 1 }}>
              {item.name}
            </Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text
              style={{
                flex: 1,
                color: '#999',
                marginTop: 5,
                marginLeft: 0,
              }}
            >
              销量{item.volume}
            </Text>
          </View>
          {type==0?
           <View style={{ flexDirection: 'row' }}>
            <Text
              style={{
                flex: 1,
                textDecorationLine:'line-through',
                color: '#999',
                marginTop: 5,
                marginLeft: 0,
              }}
            >
              ￥{item.retailPrice}
            </Text>
          </View>:null
        }
          <View style={styles.price}>
            <Text
              style={{ flex: 1, fontSize: 16, color: '#e23f3c' }}
            >{`￥${price}`}</Text>
            {type != 0 ? (
              <TouchableOpacity
                style={styles.cartButton}
                onPress={() => onCartPress && onCartPress()}
              >
                <View style={styles.cartButtonWrap}>
                  <Icon
                    name="ios-cart-outline"
                    size={16}
                    style={{ color: 'white' }}
                  />
                </View>
              </TouchableOpacity>
            ) : null}
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  innerViewStyle: {
    // 文字内容居中对齐
    alignItems: 'center',
    backgroundColor: 'white',
  },
  item: {
    paddingHorizontal: 10,
    paddingVertical: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#f0f0f0',
    borderBottomWidth: borderWidth,
  },
  title: {
    flexDirection: 'row',
    marginTop: 10,
    overflow: 'hidden',
  },
  price: {
    flexDirection: 'row',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },

  cartButton: {
    width: 40,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  cartButtonWrap: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 24,
    height: 24,
    borderColor: '#e23f3c',
    backgroundColor: '#e23f3c',
    borderRadius: 12,
    borderWidth,
  },
})
