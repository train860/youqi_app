import React, { PureComponent } from 'react'
import { View, Text, Image, Dimensions, ActivityIndicator } from 'react-native'

const { width, height } = Dimensions.get('window')

export default class Carousel extends PureComponent {
  render() {
    const { loading, text } = this.props
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        {text ? <Text>{text}</Text> : null}
        {loading === false ? null : (
          <ActivityIndicator
            style={{
              position: 'absolute',
              top: 0,
              right: 0,
              left: 0,
              bottom: 0,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          />
        )}
      </View>
    )
  }
}
