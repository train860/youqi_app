import React, { PureComponent } from 'react'
import { View, Image, TouchableWithoutFeedback, Dimensions } from 'react-native'
import Swiper from 'react-native-swiper'
import { imageServer } from '../utils'

const { width, height } = Dimensions.get('window')

export default class Carousel extends PureComponent {
  render() {
    const { data, onItemPress } = this.props
    return (
      <Swiper
        loop
        width={width}
        height={205}
        borderRadius={0}
        backgroundColor="#f6f6f6"
        paginationStyle={{ bottom: 5 }}
        dotStyle={{
          backgroundColor: 'rgba(255,255,255,.5)',
          width: 10,
          height: 1.5,
          borderRadius: 0,
        }}
        activeDotStyle={{
          backgroundColor: 'rgba(255,255,255,1)',
          width: 10,
          height: 1.5,
          borderRadius: 0,
        }}
      >
        {data.map((item, i) => (
          <View
            key={i}
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <TouchableWithoutFeedback
              onPress={() => onItemPress && onItemPress(item)}
            >
              <Image
                source={{ uri: imageServer + item.image }}
                style={{
                  height: 205,
                  width,
                  resizeMode: 'cover',
                }}
              />
            </TouchableWithoutFeedback>
          </View>
        ))}
      </Swiper>
    )
  }
}
