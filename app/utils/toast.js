import Toast from 'react-native-root-toast'

export function toast(content, timeout = 2000) {
  const toast = Toast.show(content, {
    duration: Toast.durations.LONG,
    position: Toast.positions.CENTER,
    shadow: false,
    animation: true,
    hideOnPress: true,
    delay: 0,
    onShow: () => {
      // calls on toast\`s appear animation start
    },
    onShown: () => {
      // calls on toast\`s appear animation end.
    },
    onHide: () => {
      // calls on toast\`s hide animation start.
    },
    onHidden: () => {
      // calls on toast\`s hide animation end.
    },
  })

  // You can manually hide the Toast, or it will automatically disappear after a `duration` ms timeout.
  setTimeout(() => {
    Toast.hide(toast)
  }, timeout)
}
