import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  FlatList,
  Text,
  TextInput,
  TouchableOpacity,
  DeviceEventEmitter,
  AsyncStorage,
  ScrollView,
} from 'react-native'

import Base from './Base'
import {
  isIos,
  NavigationActions,
  createAction,
  getBorderWidth,
  setAsyncStorage,
  getNowFormatDate,
} from '../utils'

import Icon from 'react-native-vector-icons/Ionicons'
import { toast } from '../utils/toast'

import RootView from '../component/RootView'

const borderWidth = getBorderWidth()

class PaySuccess extends Base {
  constructor(props) {
    super(props)
    this.state = {}
  }
  static navigationOptions = {
    title: '支付成功',
    headerBackTitle: null,
    headerTitleStyle: {
      alignSelf: 'center',
    },
    headerStyle: {
      backgroundColor: '#f9f9f9',
      borderBottomWidth: 0,
      shadowColor: 'transparent',
      elevation: 0,
    },
    headerRight: <View />,
  }
  componentDidMount() {}

  render() {
    const { order } = this.props.navigation.state.params
    let payType = '现金支付'
    let text = `您需要在货到时付款￥${order.totalPrice}`
    if (order.payType == 1) {
      payType = '支付宝'
      text = `您已成功支付￥${order.totalPrice}`
    }
    if (order.payType == 2) {
      payType = '微信支付'
      text = `您已成功支付￥${order.totalPrice}`
    }
    return (
      <ScrollView style={styles.container}>
        <View style={styles.inner}>
          <Text style={{ color: order.payType == 0 ? '#e23f3c' : '#999' }}>
            {text}
          </Text>
          <Text style={{ color: '#999', marginVertical: 10, marginBottom: 20 }}>
            我们将尽快为您配送，请耐心等待
          </Text>
          <View style={styles.header}>
            <Icon
              name="ios-checkmark-circle-outline"
              style={{ color: '#fff' }}
              size={30}
            />
            <Text
              style={{
                color: '#fff',
                fontSize: 18,
                fontWeight: 'bold',
                marginTop: 5,
              }}
            >
              ￥{order.totalPrice}
            </Text>
            <Text style={{ color: '#fff', marginTop: 15, fontSize: 12 }}>
              订单支付成功
            </Text>
          </View>
          <View style={styles.body}>
            <View
              style={[
                styles.item,
                { borderBottomWidth: borderWidth, borderColor: '#f0f0f0' },
              ]}
            >
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <Text style={{ color: '#999', fontSize: 12 }}>订单号</Text>
              </View>
              <View
                style={{
                  width: 180,
                  justifyContent: 'center',
                  alignItems: 'flex-end',
                }}
              >
                <Text style={{ color: '#999', fontSize: 12 }}>{order.id}</Text>
              </View>
            </View>
            <View
              style={[
                styles.item,
                { borderBottomWidth: borderWidth, borderColor: '#f0f0f0' },
              ]}
            >
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <Text style={{ color: '#999', fontSize: 12 }}>支付类型</Text>
              </View>
              <View
                style={{
                  width: 180,
                  justifyContent: 'center',
                  alignItems: 'flex-end',
                }}
              >
                <Text style={{ color: '#999', fontSize: 12 }}>{payType}</Text>
              </View>
            </View>
            <View style={[styles.item]}>
              <View style={{ flex: 1, justifyContent: 'center' }}>
                <Text style={{ color: '#999', fontSize: 12 }}>支付时间</Text>
              </View>
              <View
                style={{
                  width: 180,
                  justifyContent: 'center',
                  alignItems: 'flex-end',
                }}
              >
                <Text style={{ color: '#999', fontSize: 12 }}>
                  {getNowFormatDate()}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f9f9f9',
  },
  inner: {
    marginHorizontal: 20,
    marginVertical: 20,
  },
  header: {
    backgroundColor: '#0398ff',
    paddingHorizontal: 20,
    paddingVertical: 35,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
  },
  body: {
    backgroundColor: '#fff',
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
    paddingVertical: 20,
  },
  item: {
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingHorizontal: 16,
    paddingVertical: 12,
  },
})

export default PaySuccess
