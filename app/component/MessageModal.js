import React, { PureComponent } from 'react'
import {
  StyleSheet,
  Modal,
  Button,
  Text,
  View,
  TouchableOpacity,
} from 'react-native'
import { isIos, getBorderWidth } from '../utils'

const borderWidth = getBorderWidth()

export default class MessageModal extends PureComponent {
  constructor(props) {
    super(props)
  }

  render() {
    const { title, action } = this.props

    return (
      <View
        style={[styles.container, { backgroundColor: 'rgba(0, 0, 0, 0.2)' }]}
      >
        <View style={[styles.innerContainer]}>
          <View style={styles.title}>
            <Text style={{ fontWeight: 'bold' }}>{title}</Text>
          </View>
          <View style={{ paddingHorizontal: 20, alignItems: 'center' }}>
            {this.props.children}
          </View>

          <View style={styles.action}>{action.map((item, i) => item)}</View>
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 20,
    alignItems: 'center',
  },
  innerContainer: {
    borderRadius: 4,
    backgroundColor: '#fff',
    width: 260,
  },
  title: {
    paddingHorizontal: 10,
    paddingVertical: 15,
    alignItems: 'center',
  },
  action: {
    flexDirection: 'row',
    marginTop: 15,
    borderTopColor: '#e5e5e5',
    borderTopWidth: borderWidth,
  },
})
