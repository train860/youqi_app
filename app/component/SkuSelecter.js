import React, { Component } from 'react'
import {
  StyleSheet,
  Modal,
  Button,
  Text,
  View,
  TouchableOpacity,
} from 'react-native'
import { isIos, getBorderWidth } from '../utils'

const borderWidth = getBorderWidth()

export default class SkuSelecter extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activeIndex: 0,
    }
  }
  onOk() {
    const { data } = this.props
    const { activeIndex } = this.state
    this.props.onOk && this.props.onOk(data[activeIndex])
  }
  renderSpec() {
    const { activeIndex } = this.state
    return this.props.data.map((item, i) => {
      let backgroundColor = '#f6f6f6'
      let color = '#333'
      if (i == activeIndex) {
        backgroundColor = '#e23f3c'
        color = 'white'
      }
      return (
        <TouchableOpacity
          key={i}
          style={[styles.spec, { backgroundColor }]}
          onPress={() => {
            if (activeIndex != i) {
              this.setState({ activeIndex: i, buyCount: 1 })
            }
          }}
        >
          <Text style={{ color }}>{item.spec}</Text>
        </TouchableOpacity>
      )
    })
  }
  render() {
    const { title, data, onCancel, onOk } = this.props

    const item = data[this.state.activeIndex]
    let price = item.retailPrice
    if (item.salePrice > 0) {
      price = item.salePrice
    }
    return (
      <View
        style={[styles.container, { backgroundColor: 'rgba(0, 0, 0, 0.2)' }]}
      >
        <View style={[styles.innerContainer]}>
          <View style={styles.title}>
            <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{title}</Text>
          </View>
          <View style={styles.price}>
            <Text style={{ fontSize: 16 }}>¥{price}</Text>
          </View>
          <View
            style={{
              display: 'flex',
              paddingHorizontal: 20,
              paddingVertical: 10,
              flexDirection: 'row',
              flexWrap: 'wrap',
            }}
          >
            {this.renderSpec()}
          </View>

          <View style={styles.action}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => onCancel && onCancel()}
            >
              <Text style={{ color: '#666', fontWeight: 'bold' }}>取消</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={() => this.onOk()}>
              <Text style={{ color: '#e23f3c', fontWeight: 'bold' }}>确定</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 20,
    alignItems: 'center',
  },
  innerContainer: {
    borderRadius: 4,
    backgroundColor: '#fff',
    width: 300,
  },
  title: {
    paddingHorizontal: 20,
    paddingVertical: 20,
    alignItems: 'center',
  },
  price: {
    paddingHorizontal: 20,
    marginBottom: 10,
  },
  spec: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: isIos() ? 3 : 0,
    paddingVertical: 4,
    paddingHorizontal: 12,
    marginRight: 8,
    marginBottom: 8,
  },
  action: {
    flexDirection: 'row',
    marginTop: 15,
    borderTopColor: '#f0f0f0',
    borderTopWidth: borderWidth,
  },
  button: {
    flex: 1,
    paddingVertical: 15,
    paddingHorizontal: 10,
    alignItems: 'center',
  },
})
