import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  Platform,
  ScrollView,
  TextInput,
  Dimensions,
  FlatList,
} from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/Ionicons'
import TabBar from '../component/TabBar'
import OrderItem from '../component/OrderItem'
import RootView from '../component/RootView'
import Base from './Base'
import { NavigationActions, px2dp, getBorderWidth } from '../utils'
import * as orderService from '../services/order'

const { width, height } = Dimensions.get('window')

class OrderList extends Base {
  constructor(props) {
    super(props)
    this.state = {
      tabBar: [
        { key: '1', label: '全部' },
        { key: '2', label: '待付款' },
        { key: '3', label: '配送中' },
        { key: '4', label: '已完成' },
        { key: '5', label: '已取消' },
      ],
      records: [],
      times: 0,
    }
  }
  static navigationOptions = {
    title: '我的订单',
    headerBackTitle: null,
    headerTitleStyle: {
      alignSelf: 'center',
    },
    headerStyle: {
      backgroundColor: 'white',
      borderBottomWidth: 0,
      shadowColor: 'transparent',
      elevation: 1,
    },
    headerRight: <View />,
  }
  loadData(tabIndex, pageNum = 1) {
    this.setState({ fetching: true })
    const { times } = this.state
    orderService
      .page({ type: tabIndex, current: pageNum })
      .then(data => {
        this.setState({
          fetching: false,
          records: data.records,
          pages: data.pages,
          current: pageNum,
          type: tabIndex,
          times: times + 1,
        })
      })
      .catch(e => {
        this.checkError(e)
      })
  }
  onAction(screenName, item) {
    const { key } = this.props.navigation.state
    if (screenName == 'Cancel') {
      return
    }
    this.goto(screenName, { key, orderId: item.id })
  }
  onTabChange(index, item) {
    const { times } = this.state
    if (!times) {
      this.loadData(item.key)
      return
    }
    setTimeout(() => {
      this.loadData(item.key)
    }, 300)
  }
  onOrderDetail(orderId) {
    const { key } = this.props.navigation.state
    this.goto('OrderDetail', { key, orderId })
  }
  renderItem({ item, index }) {
    return (
      <OrderItem
        key={index}
        data={item}
        onAction={screenName => this.onAction(screenName, item)}
        onPress={() => this.onOrderDetail(item.id)}
      />
    )
  }
  onRefresh() {
    this.loadData(this.state.type)
  }
  onEndReached(info) {
    const { pages, current, type, records } = this.state
    const pageNum = current + 1
    if (pageNum > pages) {
      return
    }
    this.setState({ fetching: true })
    orderService
      .page({ type, current: pageNum })
      .then(data => {
        if (data.current == current) {
          return
        }
        const list = records.concat(data.records)
        this.setState({
          fetching: false,
          records: list,
          pages: data.pages,
          current: data.current,
        })
      })
      .catch(e => {
        this.checkError(e)
      })
  }
  componentWillReceiveProps(nextProps) {
    if (
      nextProps.navigation.state.params &&
      nextProps.navigation.state.params.reload
    ) {
      this.loadData(this.state.type)
    }
  }

  componentDidMount() {
    // const { type } = this.props.navigation.state.params
    // this.loadData(type)
  }

  render() {
    const { type } = this.props.navigation.state.params
    const { fetching, records, times } = this.state
    return (
      <RootView fetching={fetching} style={styles.container}>
        <TabBar
          containerWidth={width}
          tabs={this.state.tabBar}
          activeIndex={type - 1}
          init={!times}
          onTabChange={(index, item) => this.onTabChange(index, item)}
        />
        <FlatList
          refreshing={false}
          style={{ flex: 1 }}
          data={records}
          keyExtractor={(item, index) => String(index)}
          renderItem={({ item, index }) => this.renderItem({ item, index })}
          onRefresh={() => this.onRefresh()}
          onEndReached={info => this.onEndReached(info)}
          onEndReachedThreshold={0.15}
        />
      </RootView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})

export default OrderList
