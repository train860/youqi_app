import React, { Component } from 'react'
import {
  Text,
  View,
  Image,
  StyleSheet,
  Platform,
  ScrollView,
  Dimensions,
  AlertIOS,
  Animated,
  RefreshControl,
  TouchableOpacity,
  TouchableWithoutFeedback,
  FlatList,
} from 'react-native'

import { NavigationActions, px2dp, isIos } from '../utils'
import Icon from 'react-native-vector-icons/Ionicons'

const asset = require('./img/back-icon.png')

const InputHeight = px2dp(28)

export default class FixHeader extends Component {
  render() {
    const { navigation, backgroundColor, backButton } = this.props
    const { params } = navigation.state
    return (
      <View
        style={[
          styles.header,
          {
            backgroundColor: backgroundColor || '#fff',
            height: isIos() ? 64 : 56,
          },
        ]}
      >
        {backButton ? (
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ width: isIos() ? 30 : 30, justifyContent: 'center' }}
          >
            <Image style={[styles.icon]} source={asset} />
          </TouchableOpacity>
        ) : null}
        <TouchableWithoutFeedback
          style={{ flex: 1 }}
          onPress={() => params.openSearch && params.openSearch()}
        >
          <View style={[styles.searchBtn, { backgroundColor: '#f0f0f0' }]}>
            <Icon name="ios-search-outline" size={20} color="#666" />
            <Text style={{ fontSize: 13, color: '#666', marginLeft: 5 }}>
              {params.keywords ? params.keywords : '搜索商品'}
            </Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 16,
    elevation: 1,
  },
  searchBtn: {
    flex: 1,
    borderRadius: 3,
    height: InputHeight,
    flexDirection: 'row',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
  },
})
