import React, { Component } from 'react'
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
  TouchableOpacity,
  Dimensions,
  Keyboard,
} from 'react-native'

const { width, height } = Dimensions.get('window')
import Icon from 'react-native-vector-icons/Ionicons'
import { isIos } from '../utils'

const containerHeight = 28
const middleHeight = 14

export default class SearchInput extends Component {
  constructor(props) {
    super(props)

    this.state = {
      keyword: '',
    }
    this.placeholder = this.props.placeholder || 'Search'
    this.cancelTitle = this.props.cancelTitle || 'Cancel'
    this.focus = this.focus.bind(this)
  }
  /**
 * onSearch
 * async await
 */
  onSearch = async () => {
    if (this.props.keyboardShouldPersist === false) {
      await Keyboard.dismiss()
    }

    this.props.onSearch && (await this.props.onSearch(this.state.keyword))
  }
  onChangeText = async text => {
    await this.setState({ keyword: text })

    this.props.onChangeText &&
      (await this.props.onChangeText(this.state.keyword))
  }
  /**
     * onFocus
     * async await
     */
  onFocus = async () => {
    this.refs.input_keyword.isFocused && (await this.refs.input_keyword.focus())
    this.props.onFocus && (await this.props.onFocus(this.state.keyword))
  }

  /**
     * focus
     * async await
     */
  focus = async (text = '') => {
    await this.setState({ keyword: text })
    await this.refs.input_keyword.focus()
  }

  render() {
    return (
      <View
        style={[
          styles.container,
          this.props.backgroundColor && {
            backgroundColor: this.props.backgroundColor,
          },
        ]}
      >
        <TextInput
          style={[
            styles.input,
            this.props.inputHeight && { height: this.props.inputHeight },
          ]}
          ref="input_keyword"
          value={this.state.keyword}
          onChangeText={this.onChangeText}
          onSubmitEditing={this.onSearch}
          placeholder={this.placeholder}
          placeholderTextColor={
            this.props.placeholderTextColor || styles.placeholderColor
          }
          onFocus={this.onFocus}
          underlineColorAndroid="transparent"
          autoCorrect={false}
        />
        <TouchableWithoutFeedback onPress={this.onFocus}>
          <Image
            source={require('./img/search.png')}
            style={[
              styles.iconSearch,
              styles.iconSearchDefault,

              {
                left: 10,
              },
            ]}
          />
        </TouchableWithoutFeedback>
      </View>
    )
  }
}
SearchInput.defaultProps = {
  keyboardShouldPersist: false,
}
const styles = StyleSheet.create({
  container: {
    height: containerHeight,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: 5,
  },
  input: {
    flex: 1,
    height: containerHeight,
    paddingLeft: 25,
    paddingTop: 5,
    paddingBottom: 5,
    paddingRight: 20,
    borderColor: '#444',
    backgroundColor: '#f7f7f7',
    borderRadius: 3,
    fontSize: 13,
  },
  // placeholderColor: 'grey',
  iconSearch: {
    flex: 1,
    position: 'absolute',
    top: middleHeight - 7,
    height: 14,
    width: 14,
  },
  iconSearchDefault: {
    tintColor: 'grey',
  },
  iconDelete: {
    position: 'absolute',
    right: 10,
    top: middleHeight - 7,
    height: 14,
    width: 14,
  },
  iconDeleteDefault: {
    tintColor: 'grey',
  },
})
