import { request } from '../utils'

export const login = async params =>
  request({
    url: 'sys/user/signin',
    method: 'post',
    data: params,
    token: false,
  })

export const captcha = async params =>
  request({
    url: 'sys/user/captcha',
    method: 'post',
    data: params,
    token: false,
  })

export const signup = async params =>
  request({
    url: 'sys/user/signup',
    method: 'post',
    data: params,
    token: false,
  })
export const reset = async params =>
  request({
    url: 'sys/user/reset',
    method: 'post',
    data: params,
    token: false,
  })
