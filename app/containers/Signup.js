import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native'
import { connect } from 'react-redux'

import Base from './Base'
import {
  NavigationActions,
  createAction,
  setAsyncStorage,
  removeAsyncStorage,
} from '../utils'
import { toast } from '../utils/toast'
import CheckBox from '../component/CheckBox'
import Button from '../component/Button'
import RootView from '../component/RootView'
import * as authService from '../services/auth'

class Signup extends Base {
  static navigationOptions = {
    title: '手机号',
    headerBackTitle: null,
    headerStyle: {
      backgroundColor: 'white',
      borderBottomWidth: 0,
      shadowColor: 'transparent',
      elevation: 1,
    },
    headerRight: <View />,
  }

  onSubmit = () => {
    const mobile = this.refs.mobile._lastNativeText
    this.setState({ fetching: true })
    authService
      .captcha({ mobile })
      .then(data => {
        this.setState({ fetching: false })
        let key = ''
        const { params } = this.props.navigation.state
        if (params && params.key) {
          key = params.key
        }
        this.goto('Password', { mobile, key })
      })
      .catch(e => {
        this.setState({ fetching: false })
        toast(e.msg)
      })
  }
  componentWillUnmount() {
    removeAsyncStorage('Signup')
  }
  componentDidMount() {
    const { key } = this.props.navigation.state
    console.log(key)
    setAsyncStorage('Signup', key)
  }
  render() {
    const { fetching, msg } = this.state
    return (
      <RootView fetching={fetching} style={styles.container}>
        <ScrollView>
          <View style={styles.textInputWrap}>
            <TextInput
              ref="mobile"
              underlineColorAndroid="transparent"
              keyboardType={'numeric'}
              style={styles.textInput}
              placeholder="请输入手机号码"
              placeholderTextColor="#aaa"
            />
          </View>
          {/**
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 10,
              paddingHorizontal: 2,
            }}
          >
            <CheckBox checked color="#0398ff" width={18} height={18} />
            <Text style={{ color: '#999', marginLeft: 5 }}>同意</Text>
            <TouchableOpacity>
              <Text>优琦用户使用协议</Text>
            </TouchableOpacity>
          </View>
        * */}
          <Button
            style={{ marginTop: 20, borderRadius: 3, overflow: 'hidden' }}
            onPress={() => {
              this.onSubmit()
            }}
          >
            <View
              style={{
                flex: 1,
                height: 40,
                backgroundColor: '#56d176',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Text style={{ color: '#fff' }}>{'下一步'}</Text>
            </View>
          </Button>
        </ScrollView>
      </RootView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
    backgroundColor: '#f6f6f6',
  },
  textInputWrap: {
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
    borderWidth: 1,
    borderColor: '#f0f0f0',
  },
  textInput: {
    flex: 1,
    paddingVertical: 0,
    height: 36,
    fontSize: 14,
    paddingHorizontal: 10,
  },
})

export default Signup
