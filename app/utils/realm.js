import Realm from 'realm'

class Search extends Realm.Object {}
Search.schema = {
  name: 'Search',
  primaryKey: 'id',
  properties: {
    id: 'string',
    value: 'string',
  },
}
class Cart extends Realm.Object {}
Cart.schema = {
  name: 'Cart',
  primaryKey: 'id',
  properties: {
    id: 'string',
    num: { type: 'int', default: 1 },
    checked: 'bool',
  },
}

export default new Realm({ schema: [Search, Cart] })
