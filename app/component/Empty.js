import React, { PureComponent } from 'react'
import { View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

export default class Empty extends PureComponent {
  render() {
    const { text } = this.props

    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Icon name={'ios-warning-outline'} size={36} color="#999" />
        <Text style={{ color: '#999', marginTop: 10 }}>
          {text || '未找到相关内容'}
        </Text>
      </View>
    )
  }
}
