import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  FlatList,
  Text,
  TextInput,
  TouchableOpacity,
  Modal,
  NativeAppEventEmitter,
  DeviceEventEmitter,
  AsyncStorage,
} from 'react-native'

import Base from './Base'
import {
  isIos,
  NavigationActions,
  createAction,
  getBorderWidth,
  setAsyncStorage,
} from '../utils'

import Icon from 'react-native-vector-icons/Ionicons'
import AMapLocation from 'react-native-amap-location'
import SystemSetting from 'react-native-system-setting'
import { toast } from '../utils/toast'
import Empty from '../component/Empty'
import CheckBox from '../component/CheckBox'
import Button from '../component/Button'
import MessageModal from '../component/MessageModal'
import RootView from '../component/RootView'
import SearchInput from '../component/SearchInput'
import * as orderService from '../services/order'

const borderWidth = getBorderWidth()

class Coupon extends Base {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      requests:0,
    }

  }
  static navigationOptions = {
    title: '选择优惠券',
    headerBackTitle: null,
    headerTitleStyle: {
      alignSelf: 'center',
    },
    headerStyle: {
      backgroundColor: '#f9f9f9',
      borderBottomWidth: 0,
      shadowColor: 'transparent',
      elevation: 0,
    },
    headerRight: <View />,
  }
  loadData() {
      this.setState({ fetching: true })
      let {requests}=this.state
      requests++
      const newData=[]
      const { amount,couponId } = this.props.navigation.state.params

      orderService
      .coupons({})
      .then(data => {
        
        let i=0
        data.map((item)=>{
            if(item.condition>amount){
              return
            }
            if(item.id==couponId){
              item.selected=1
              i++
            }
            newData.push(item)
        })
        if(data.length>0){
          if(i==0){
            newData.push({id:-1,selected:1})
          }else{
            newData.push({id:-1})
          }
        }
        
        this.setState({
          fetching: false,
          data:newData,
          requests,
        })
      })
      .catch(e => {
        this.checkError(e,{requests})
      })
  }

  onItemPress(item) {

    const { key } = this.props.navigation.state.params

    this.setParams(key, { couponId:item.id })
    setTimeout(() => {
      this.goBack()
    }, 50)
  }

 
  renderItem = ({ item, index }) => {
    let borderTopWidth = borderWidth
    if (index == 0) {
      borderTopWidth = 0
    }
    let text=''
    if(item.condition<=0){
      text=item.amount+'元代金券'
    }else{
      text='满'+item.condition+'减'+item.amount+'代金券'
    }
    if(item.id<0){
      return (
        <TouchableOpacity
          activeOpacity={0.85}
          onPress={() => this.onItemPress(item)}>
          <View style={styles.action}>
              <CheckBox
                  onPress={() => this.onItemPress(item)}
                  checked={item.selected}
                  color="#e23f3c"/>
                <Text style={{ color: '#666',marginLeft: 5 }}>
                  不使用优惠券
                </Text>
            </View>
        </TouchableOpacity>
      )
    }
    return (
      <TouchableOpacity
        activeOpacity={0.85}
        onPress={() => this.onItemPress(item)}>
        <View style={styles.action}>
            <CheckBox
                checked={item.selected}
                color="#e23f3c"/>
              <Text style={{ color: '#666',marginLeft: 5 }}>
                {text}
              </Text>
          </View>
      </TouchableOpacity>
    )
  }

  componentWillUnmount() {
    
  }
  componentDidMount() {
     this.loadData()
  }

  render() {
    const { fetching, data,requests } = this.state
    let size = 0
    if (data) {
      size = data.length
    }
    return (
      <RootView fetching={fetching} style={styles.container}>
        
          {size > 0 ? (
            <FlatList
              style={{ flex: 1, marginTop: 15 }}
              horizontal={false}
              keyExtractor={(item, index) => String(index)}
              data={data}
              renderItem={this.renderItem}
            />
          ) : null}
          {size==0&&requests>0?
            <Empty text='没有可以使用的优惠券'/>:null
          }
      </RootView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  action: {
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#fbfbfb',
    paddingHorizontal: 16,
    backgroundColor: '#fff',
    paddingVertical: 8,
  },
})

export default Coupon
