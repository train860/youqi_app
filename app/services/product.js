import { request } from '../utils'

export const list = async params =>
  request({
    url: 'api/v1/goods/list',
    method: 'get',
    data: params,
    token: false,
  })

export const items = async params =>
  request({
    url: 'api/v1/product/items',
    method: 'get',
    data: params,
    token: false,
  })
export const skus = async params =>
  request({
    url: 'api/v1/goods/items',
    method: 'get',
    data: params,
    token: false,
  })
export const detail = params => {
  if (params.isSku == undefined) {
    params.isSku = 0
  }
  return request({
    url: 'api/v1/goods/info',
    method: 'get',
    data: params,
    token: false,
  })
}
export const recommend = async params => {
  if (params.type == undefined) {
    params.type = 1
  }
  return request({
    url: 'api/v1/recommend/list',
    method: 'get',
    data: params,
    token: false,
  })
}
