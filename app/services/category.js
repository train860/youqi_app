import { request } from '../utils'

export const categoryA = async params =>
  request({
    url: 'api/v1/category/a',
    method: 'get',
    data: params,
    token: false,
  })
export const categoryB = async params =>
  request({
    url: 'api/v1/category/b',
    method: 'get',
    data: params,
    token: false,
  })
