/**
 * @author Lei
 * @repo https://github.com/stoneWeb/elm-react-native
 */

import React, { Component } from 'react'
import {
  View,
  Text,
  Easing,
  Animated,
  Platform,
  StyleSheet,
  Dimensions,
  ScrollView,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native'

import CheckBox from './CheckBox'
import Icon from 'react-native-vector-icons/Ionicons'

import { px2dp } from '../utils'

const { width, height } = Dimensions.get('window')

export default class CartBar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      scale: new Animated.Value(0),
    }
  }
  runAnimate() {
    this.state.scale.setValue(0)
    Animated.timing(this.state.scale, {
      toValue: 2,
      duration: 320,
      easing: Easing.elastic(3),
    }).start()
  }
  render() {
    const { totalPrice, all, onCheckAll } = this.props

    return (
      <View style={styles.cartView}>
        <View style={styles.cartBar}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              paddingHorizontal: 16,
            }}
          >
            <CheckBox
              checked={all}
              color="#e23f3c"
              onPress={() => onCheckAll(all)}
            />
            <Text style={{ color: '#666', fontSize: 10, marginLeft: 5 }}>
              全选
            </Text>
          </View>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            <Text style={{ color: '#666' }}>合计：￥{totalPrice || '0.0'}</Text>
          </View>
          <TouchableOpacity onPress={() => this.props.onCheckIn()}>
            <Text
              style={[
                styles.price,
                { backgroundColor: '#e23f3c', color: '#fff' },
              ]}
            >
              {'去结算'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  cartView: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    right: 0,
    width,
    height: px2dp(46) + 10,
  },
  count: {
    backgroundColor: '#f00',
    height: px2dp(12),
    borderRadius: 5,
    overflow: 'hidden',
    paddingHorizontal: 4,
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    right: 0,
  },
  iconWrap: {
    position: 'absolute',
    left: 16,
    top: 0,
    width: px2dp(46),
    height: px2dp(46),
  },
  iconView: {
    backgroundColor: '#333',
    overflow: 'hidden',
    borderRadius: px2dp(23),
    width: px2dp(46),
    height: px2dp(46),
    borderWidth: 4,
    borderColor: '#555',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cartBar: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    right: 0,
    width,
    height: px2dp(46),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  price: {
    color: '#999',
    fontSize: px2dp(16),
    paddingHorizontal: 20,
    height: px2dp(46),
    lineHeight: Platform.OS === 'ios' ? px2dp(46) : 32,
    backgroundColor: 'rgba(255,255,255,.1)',
  },
  blur: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
})
