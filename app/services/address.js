import { request } from '../utils'

export const detail = params => {
  const url = `api/v1/address/info/${params.id}`
  return {
    url,
    method: 'get',
    data: {},
  }
}
export const save = params =>
  request({
    url: 'api/v1/address/save',
    method: 'post',
    data: params,
  })
export const update = params =>
  request({
    url: 'api/v1/address/update',
    method: 'post',
    data: params,
  })
export const setDefault = params =>
  request({
    url: 'api/v1/address/setDefault',
    method: 'post',
    data: params,
  })
export const remove = params => {
  const url = `api/v1/address/remove/${params.id}`
  return request({
    url,
    method: 'post',
    data: {},
  })
}
export const list = params =>
  request({
    url: 'api/v1/address/list',
    method: 'post',
    data: params,
  })
