import React, { PureComponent } from 'react'
import { StyleSheet, Modal, Button, Text, View } from 'react-native'

export default class Message extends PureComponent {
  constructor(props) {
    super(props)
  }
  render() {
    const { visible, onClose } = this.props
    return (
      <Modal animationType={'fade'} transparent visible={visible}>
        <View
          style={[styles.container, { backgroundColor: 'rgba(0, 0, 0, 0.5)' }]}
        >
          <View
            style={[
              styles.innerContainer,
              { backgroundColor: '#fff', padding: 20 },
            ]}
          >
            <View style={{ marginBottom: 10 }}>{this.props.children}</View>
            <Button
              title="Close"
              onPress={() => onClose()}
              style={styles.modalButton}
            />
          </View>
        </View>
      </Modal>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 20,
  },
  innerContainer: {
    borderRadius: 10,
    alignItems: 'center',
  },
  modalButton: {},
})
