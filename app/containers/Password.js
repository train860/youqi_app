import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native'
import { connect } from 'react-redux'

import Base from './Base'
import {
  NavigationActions,
  createAction,
  getBorderWidth,
  getAsyncStorage,
} from '../utils'
import { toast } from '../utils/toast'
import Button from '../component/Button'
import RootView from '../component/RootView'
import * as authService from '../services/auth'

class Password extends Base {
  static navigationOptions = {
    title: '填写密码',
    headerBackTitle: null,
    headerStyle: {
      backgroundColor: 'white',
      borderBottomWidth: 0,
      shadowColor: 'transparent',
      elevation: 1,
    },
  }

  onSubmit = async () => {
    const signupKey = await getAsyncStorage('Signup')

    this.setState({ fetching: true })
    const mobile = this.refs.mobile._lastNativeText
    const captcha = this.refs.captcha._lastNativeText
    const password = this.refs.password._lastNativeText
    const { key } = this.props.navigation.state.params
    const { navigation } = this.props
    if (key == 'reset') {
      authService
        .reset({ mobile, captcha, password })
        .then(data => {
          this.setState({ fetching: false })
          // 重置密码
          toast('重置密码成功')
          setTimeout(() => {
            this.goBack(signupKey)
          }, 2000)
        })
        .catch(e => {
          this.setState({ fetching: false })
          toast(e.msg)
        })
      return
    }
    authService
      .signup({ mobile, captcha, password })
      .then(data => {
        this.setState({ fetching: false })
        // 注册成功
        toast('注册成功')
        setTimeout(() => {
          this.goBack(signupKey)
        }, 2000)
      })
      .catch(e => {
        this.setState({ fetching: false })
        toast(e.msg)
      })
  }

  render() {
    const { mobile } = this.props.navigation.state.params
    const { fetching } = this.state

    return (
      <RootView
        fetching={fetching}
        onModalClose={() => this.onModalClose()}
        style={styles.container}
      >
        <ScrollView>
          <View
            style={{ marginTop: 10, backgroundColor: '#fff', paddingLeft: 16 }}
          >
            <View style={styles.item}>
              <View style={styles.label}>
                <Text>{'账号'}</Text>
              </View>
              <View style={{ flex: 1 }}>
                <TextInput
                  ref="mobile"
                  editable={false}
                  value={mobile}
                  underlineColorAndroid="transparent"
                  keyboardType={'numeric'}
                  style={styles.textInput}
                  placeholder="手机号码"
                  placeholderTextColor="#aaa"
                />
              </View>
            </View>
            <View style={styles.item}>
              <View style={styles.label}>
                <Text>{'验证码'}</Text>
              </View>
              <View style={{ flex: 1 }}>
                <TextInput
                  ref="captcha"
                  underlineColorAndroid="transparent"
                  keyboardType={'numeric'}
                  style={styles.textInput}
                  placeholder="验证码"
                  placeholderTextColor="#aaa"
                />
              </View>
            </View>
            <View style={styles.item}>
              <View style={styles.label}>
                <Text>{'密码'}</Text>
              </View>
              <View style={{ flex: 1 }}>
                <TextInput
                  ref="password"
                  secureTextEntry
                  underlineColorAndroid="transparent"
                  style={styles.textInput}
                  placeholder="登录密码"
                  placeholderTextColor="#aaa"
                />
              </View>
            </View>
          </View>

          <Button
            style={{
              marginTop: 20,
              marginHorizontal: 16,
              borderRadius: 3,
              overflow: 'hidden',
            }}
            onPress={() => {
              this.onSubmit()
            }}
          >
            <View
              style={{
                flex: 1,
                height: 40,
                backgroundColor: '#56d176',
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Text style={{ color: '#fff' }}>{'确定'}</Text>
            </View>
          </Button>
        </ScrollView>
      </RootView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  item: {
    borderBottomWidth: getBorderWidth(),
    borderBottomColor: '#f0f0f0',
    paddingVertical: 10,
    flexDirection: 'row',
  },
  active: {
    borderColor: '#81c2ff',
    color: '#0096ff',
  },
  label: {
    minWidth: 45,
    height: 28,
    justifyContent: 'center',
  },
  textInput: {
    flex: 1,
    paddingVertical: 0,
    height: 30,
    fontSize: 14,
    paddingHorizontal: 10,
  },
})

export default Password
