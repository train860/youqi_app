import { request } from '../utils'

export const home = async params =>
  request({
    url: 'api/v1/app/home',
    method: 'get',
    data: params,
    net: true,
    token: false,
  })
export const suggest = async params =>
  request({
    url: 'api/v1/suggestion/save',
    method: 'post',
    data: params,
  })
export const version = async params =>
  request({
    url: 'api/v1/appVersion/last',
    method: 'get',
    data: params,
    token: false,
  })

export const config = async params =>
  request({
    url: 'api/v1/appConfig/all',
    method: 'get',
    data: params,
    token: false,
  })
