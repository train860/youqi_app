import React, { Component } from 'react'
import {
  Text,
  View,
  Image,
  StyleSheet,
  ScrollView,
  Dimensions,
  Platform,
  PixelRatio,
  TextInput,
  Linking,
  Alert,
  TouchableOpacity,
  TouchableHighlight,
  TouchableNativeFeedback,
  TouchableWithoutFeedback,
} from 'react-native'
import Immutable from 'immutable'
import Icon from 'react-native-vector-icons/Ionicons'

import Item from '../component/Item'
import RootView from '../component/RootView'
import Base from './Base'
import LocalImg from '../images'
import {
  NavigationActions,
  px2dp,
  getBorderWidth,
  createAction,
  removeAsyncStorage,
  isIos,
  getVersion,
} from '../utils'
import { toast } from '../utils/toast'
import * as appService from '../services/app'

const { width, height } = Dimensions.get('window')

const borderWidth = getBorderWidth()

class About extends Base {
  constructor(props) {
    super(props)
    this.state = {
      modalVisible: false,
      fetching: false,
      msg: null,
      csh: '', // 客服电话
    }
    this.config = [
      {
        icon: 'ios-phone-portrait-outline',
        name: '版本检查',
        first: true,
        onPress: () => {
          this.checkVersion()
        },
      },
      {
        icon: 'ios-call-outline',
        name: '我的客服',
        color: '#fc7b53',
        onPress: () => {
          this.openTel()
        },
      },
    ]
  }
  static navigationOptions = {
    title: '关于',
    headerBackTitle: null,
    headerTitleStyle: {
      alignSelf: 'center',
    },
    headerStyle: {
      backgroundColor: '#fff',
      borderBottomWidth: 0,
      shadowColor: 'transparent',
      elevation: 0,
    },
    headerRight: <View />,
  }
  loadData = () => {
    this.setState({ fetching: true })
    appService
      .config({})
      .then(data => {
        if (data.csh) {
          this.setState({ csh: data.csh, fetching: false })
        }
      })
      .catch(e => {
        this.checkError(e)
      })
  }
  checkVersion = () => {
    this.setState({ fetching: true })
    const type = isIos() ? 1 : 0
    const version = getVersion().replace('.', '')
    appService
      .version({ type })
      .then(data => {
        this.setState({ fetching: false })
        const last = data.version.version
        if (last) {
          const number = last.replace('.', '')
          if (version < number) {
            // 发现新版本
            Alert.alert('新版本提醒', data.version.content, [
              {
                text: '取消',
                style: 'cancel',
              },
              { text: '去更新', onPress: () => this.linkTo(data.version.link) },
            ])

            return
          }
        }
        // 没有新版本
        toast('当前已经是最新版本')
      })
      .catch(e => {
        this.setState({ fetching: false })
        toast('当前已经是最新版本')
      })
  }
  openTel = () => {
    const { csh } = this.state

    if (!csh) {
      return
    }
    const url = `tel:${csh}`
    this.linkTo(url)
  }
  linkTo = url => {
    Linking.canOpenURL(url)
      .then(supported => {
        if (!supported) {
        } else {
          return Linking.openURL(url)
        }
      })
      .catch(err => console.error('An error occurred', err))
  }

  renderListItem() {
    return this.config.map((item, i) => <Item key={i} {...item} />)
  }
  componentDidMount() {
    this.loadData()
  }
  render() {
    const { fetching } = this.state
    const version = getVersion()
    return (
      <RootView fetching={fetching} style={styles.container}>
        <View
          style={{
            padding: 20,
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Image source={LocalImg.logo} style={{ width: 80, height: 80 }} />
          <Text style={{ marginTop: 10, color: '#999' }}>版本号 v{version}</Text>
        </View>
        {this.renderListItem()}
        <View style={styles.copyright}>
          <Text style={{ color: '#999', fontSize: 12 }}>
            Copyright 2017-2019
          </Text>
          <Text style={{ color: '#999', fontSize: 12, marginTop: 5 }}>
            甘肃优琦商贸有限公司
          </Text>
        </View>
      </RootView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f6f6f6',
  },
  copyright: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: 'center',
    paddingVertical: 15,
  },
})

export default About
