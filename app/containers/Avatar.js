import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  DeviceEventEmitter,
  AsyncStorage,
} from 'react-native'

import Base from './Base'
import {
  isIos,
  NavigationActions,
  createAction,
  getBorderWidth,
  setAsyncStorage,
  getAsyncStorage,
} from '../utils'
import Icon from 'react-native-vector-icons/Ionicons'
import RootView from '../component/RootView'
import Button from '../component/Button'
import QRCode from 'react-native-qrcode'

class CloseButton extends Component {
  render() {
    return (
      <TouchableOpacity onPress={() => this.props.onPress()}>
        <View
          style={{ flex: 1, justifyContent: 'center', paddingHorizontal: 16 }}
        >
          <Icon name="ios-close" size={36} />
        </View>
      </TouchableOpacity>
    )
  }
}

class Avatar extends Base {
  constructor(props) {
    super(props)
    this.state = {
      count: 0,
    }
    this.interval = null
  }
  renderHeader() {

    return (
      <View style={{ height: isIos() ? 64 : 64, backgroundColor: 'white' }}>
        <View
          style={{
            marginTop: isIos() ? 20 : 0,
            flex: 1,
            paddingHorizontal: 16,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <TouchableOpacity
            onPress={() => this.goBack()}
            style={{ width: 36, justifyContent: 'center' }}
          >
            <Icon name="ios-close" size={36} style={{ color: '#333' }} />
          </TouchableOpacity>

          <View
            style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
          >
            <Text numberOfLines={1} style={{fontSize:16,fontWeight:'bold'}}>二维码</Text>
          </View>
          <View style={{ width: 36 }} />
        </View>
      </View>
    )
  }


  componentDidMount() {
    
  }

  render() {
    const { key } = this.props.navigation.state.params

    return (
     <RootView style={styles.container}>

        {this.renderHeader()}
         <ScrollView style={{backgroundColor: 'white',flex:1}}>
         <View style={styles.wrap}>
            <QRCode
              value={key}
              size={240}
              bgColor='black'
              fgColor='white'/>
          </View>
          </ScrollView>

      </RootView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  wrap:{
    
    flex:1,alignItems:'center',
    marginTop:80,
  }
})

export default Avatar
