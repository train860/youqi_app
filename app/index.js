import React from 'react'
import { AppRegistry, AsyncStorage } from 'react-native'
import Router from './router'

AppRegistry.registerComponent('YouQi', () => Router)
