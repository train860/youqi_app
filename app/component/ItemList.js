/**
 * by train
 * 订单详情中商品列表
 */

import React, { Component, PropTypes } from 'react'
import {
  View,
  Image,
  StyleSheet,
  Dimensions,
  Platform,
  TouchableHighlight,
  AlertIOS,
  TouchableNativeFeedback,
} from 'react-native'

import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Text,
  Body,
  Right,
} from 'native-base'
import { imageServer } from '../utils'

export default class ItemList extends Component {
  constructor(props) {
    super(props)
  }
  _renderItem() {
    const { data } = this.props
    console.log(data)
    return data.map((item, index) => (
      <ListItem
        key={index}
        style={{ borderColor: '#f0f0f0', paddingTop: 5, paddingBottom: 5 }}
      >
        <Image
          style={{ width: 40, height: 40 }}
          source={{
            uri:
              imageServer +
              (item.get('imageSource')
                ? item.get('imageSource')
                : item.get('product_image')),
          }}
        />
        <Body>
          <Text numberOfLines={1} style={{ fontSize: 14 }}>
            {item.get('productName')}
          </Text>
          <Text style={{ fontSize: 12, marginTop: 5, color: '#888' }}>
            {item.get('spec')}
          </Text>
        </Body>
        <Right>
          <Text note>
            ¥{item.get('price')}x{item.get('num')}
          </Text>
        </Right>
      </ListItem>
    ))
  }
  render() {
    return (
      <List style={{ backgroundColor: 'white' }}>{this._renderItem()}</List>
    )
  }
}

const styles = StyleSheet.create({})
