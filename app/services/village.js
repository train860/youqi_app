import { request } from '../utils'

export const list = async params =>
  request({
    url: 'api/v1/village/list',
    method: 'get',
    data: params,
    token: false,
  })
