import { request } from '../utils'

export const view = params =>
  request({
    url: 'api/v1/order/view',
    method: 'post',
    data: params,
  })
export const detail = params =>
  request({
    url: 'api/v1/order/detail',
    method: 'post',
    data: params,
  })
export const create = (params) =>{
  if(!params.couponId){
    params.couponId=-1
  }
  return request({
    url: 'api/v1/order/create',
    method: 'post',
    data: params,
  })
}
export const cancel = params =>
  request({
    url: 'api/v1/order/cancel',
    method: 'post',
    data: params,
  })
export const comment = params =>
  request({
    url: 'api/v1/comment/save',
    method: 'post',
    data: params,
  })
export const page = params =>
  request({
    url: 'api/v1/order/list',
    method: 'post',
    data: params,
  })
export const cashPay = params =>
  request({
    url: 'api/v1/order/cashPay',
    method: 'post',
    data: params,
  })
export const alipay = params =>
  request({
    url: 'api/v1/order/alipayPackage',
    method: 'post',
    data: params,
  })
export const wxpay = params =>
  request({
    url: 'api/v1/order/wxpayPackage',
    method: 'post',
    data: params,
  })

export const coupons= params =>
  request({
    url: 'api/v1/coupon/list',
    method: 'get',
    data: params,
  })
