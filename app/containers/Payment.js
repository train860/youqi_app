import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  Platform,
  ScrollView,
  Image,
  TextInput,
} from 'react-native'
import {
  Container,
  Header,
  Thumbnail,
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Switch,
} from 'native-base'
import Icon from 'react-native-vector-icons/Ionicons'
import Alipay from 'react-native-yunpeng-alipay'
import * as WeChat from 'react-native-wechat'
import RootView from '../component/RootView'
import LocalImg from '../images'
import Base from './Base'
import { NavigationActions, px2dp } from '../utils'
import * as orderService from '../services/order'

class Payment extends Base {
  constructor(props) {
    super(props)
    this.state = {
      order: {},
    }
  }
  static navigationOptions = {
    title: '收银台',
    headerBackTitle: null,
    headerTitleStyle: {
      alignSelf: 'center',
    },
    headerStyle: {
      backgroundColor: 'white',
      borderBottomWidth: 0,
      shadowColor: 'transparent',
      elevation: 1,
    },
    headerRight: <View />,
  }
  loadData(orderId) {
    this.setState({ fetching: true })
    orderService
      .detail({ orderId })
      .then(data => {
        this.setState({
          fetching: false,
          order: data.order,
        })
      })
      .catch(e => {
        this.checkError(e)
      })
  }
  wxpay() {
    this.setState({ fetching: true })

    const { id } = this.state.order
    orderService
      .wxpay({ orderId: id })
      .then(data => {
        // 操作成功
        WeChat.pay(data.data)
          .then(result => {
            this.setState({ fetching: false })
            const { order } = this.state
            order.payType = 2
            this.goto('PaySuccess', { order })
          })
          .catch(e => {
            this.setState({ fetching: false })
            //this.checkError({ msg:JSON.stringify(e) })
          })
      })
      .catch(e => {
        this.checkError(e)
      })
  }
  alipay() {
    this.setState({ fetching: true })
    const { id } = this.state.order
    orderService
      .alipay({ orderId: id })
      .then(data => {
        // 操作成功

        Alipay.pay(data.data).then(
          data => {
            this.setState({ fetching: false })
            const { order } = this.state
            order.payType = 1
            this.goto('PaySuccess', { order })
          },
          err => {
            this.checkError({ msg: '订单支付失败' })
          }
        )
      })
      .catch(e => {
        this.checkError(e)
      })
  }
  cashpay() {
    this.setState({ fetching: true })
    const { id } = this.state.order
    orderService
      .cashPay({ orderId: id })
      .then(data => {
        // 操作成功
        this.setState({ fetching: false })
        this.goto('PaySuccess', { order: this.state.order })
      })
      .catch(e => {
        this.checkError(e)
      })
  }
  async componentDidMount() {
    const { orderId } = this.props.navigation.state.params
    await WeChat.registerApp('wxa5712dc7a9d1c367')
    this.loadData(orderId)
  }

  render() {
    const { fetching, order } = this.state
    return (
      <RootView fetching={fetching} style={styles.container}>
        <Content>
          <List style={{ marginTop: 10 }}>
            <ListItem style={styles.priceItem} onPress={() => {}}>
              <Body>
                <Text style={{ color: '#999' }}>订单金额</Text>
              </Body>
              <Right>
                <Text style={{ color: '#e23f3c' }}>¥{order.totalPrice}</Text>
              </Right>
            </ListItem>
            <ListItem icon style={styles.item} onPress={() => this.alipay()}>
              <Left>
                <Image
                  source={LocalImg.alipay}
                  style={{ width: 20, height: 20 }}
                />
              </Left>
              <Body>
                <Text>支付宝支付</Text>
                <Text style={{ fontSize: 10, color: '#999', marginTop: 5 }}>
                  支付宝安全支付
                </Text>
              </Body>
              <Right>
                <Icon
                  name="ios-arrow-forward"
                  size={18}
                  style={{ color: '#999' }}
                />
              </Right>
            </ListItem>
            <ListItem icon style={styles.item} onPress={() => this.wxpay()}>
              <Left>
                <Image
                  source={LocalImg.wechat}
                  style={{ width: 20, height: 18 }}
                />
              </Left>
              <Body>
                <Text>微信支付</Text>
                <Text style={{ fontSize: 10, color: '#999', marginTop: 5 }}>
                  微信安全支付
                </Text>
              </Body>
              <Right>
                <Icon
                  name="ios-arrow-forward"
                  size={18}
                  style={{ color: '#999' }}
                />
              </Right>
            </ListItem>
            <ListItem icon style={styles.item} onPress={() => this.cashpay()}>
              <Left>
                <Image
                  source={LocalImg.cash}
                  style={{ width: 20, height: 18 }}
                />
              </Left>
              <Body>
                <Text>货到付款</Text>
                <Text style={{ fontSize: 10, color: '#999', marginTop: 5 }}>
                  现金支付
                </Text>
              </Body>
              <Right>
                <Icon
                  name="ios-arrow-forward"
                  size={18}
                  style={{ color: '#999' }}
                />
              </Right>
            </ListItem>
          </List>
          <View style={styles.note}>
            <Text style={styles.noteText}>请在15分钟内完成支付</Text>
          </View>
        </Content>
      </RootView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
  },
  priceItem: {
    marginLeft: 0,
    paddingLeft: 16,
  },
  item: {
    marginLeft: 0,
    paddingLeft: 16,
    borderColor: '#f6f6f6',
  },
  note: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  noteText: {
    color: '#999',
    fontSize: 12,
  },
})

export default Payment
