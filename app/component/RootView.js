/**
 * @author Lei
 * @repo https://github.com/stoneWeb/elm-react-native
 */

import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Text,
  ActivityIndicator,
  NetInfo,
  Alert,
  StatusBar,
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import Button from '../component/Button'
import Message from './Message'

export default class RootView extends Component {
  constructor(props) {
    super(props)
  }
  reload() {
    NetInfo.isConnected.fetch().then(isConnected => {
      if (!isConnected) {
        Alert.alert('', '请开启网络后刷新', [
          {
            text: '确定',
            onPress: () => {},
            style: 'cancel',
          },
        ])
        return
      }
      this.props.onReconnect && this.props.onReconnect()
    })
  }
  componentDidMount() {
    let { barStyle, nobar } = this.props
    if (!nobar) {
      if (!barStyle) {
        barStyle = 'default'
      }
      StatusBar.setBarStyle(barStyle)
    }
  }
  render() {
    const { fetching, notConnected } = this.props
    if (notConnected) {
      return (
        <View style={styles.container}>
          <Icon name="ios-wifi-outline" size={48} color="#999" />
          <Text style={{ marginTop: 10, color: '#999' }}>当前网络不可用，请开启网络后重试</Text>
          <Button
            style={{
              height: 45,
              marginTop: 5,
            }}
            onPress={() => this.reload()}
          >
            <Text style={{ color: '#0096ff' }}>{'点击刷新'}</Text>
          </Button>
        </View>
      )
    }
    return (
      <View style={[styles.warp, this.props.style]}>
        {this.props.children}

        {fetching ? (
          <ActivityIndicator
            style={{
              position: 'absolute',
              top: 0,
              right: 0,
              left: 0,
              bottom: 0,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          />
        ) : null}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  warp: {
    flex: 1,
  },
})
