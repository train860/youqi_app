import React, { Component } from 'react'
import {
  Text,
  View,
  Image,
  StyleSheet,
  ScrollView,
  Dimensions,
  Platform,
  PixelRatio,
  TextInput,
} from 'react-native'
import Immutable from 'immutable'
import Icon from 'react-native-vector-icons/Ionicons'

import RootView from '../component/RootView'
import Button from '../component/Button'
import Base from './Base'
import {
  NavigationActions,
  px2dp,
  createAction,
  removeAsyncStorage,
} from '../utils'
import * as appService from '../services/app'

const { width, height } = Dimensions.get('window')

const borderWidth = (Platform.OS === 'ios' ? 1.0 : 1.0) / PixelRatio.get()

class Suggestion extends Base {
  constructor(props) {
    super(props)
  }
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state
    return {
      title: '意见或建议',
      headerBackTitle: null,
      headerTitleStyle: {
        alignSelf: 'center',
      },
      headerStyle: {
        backgroundColor: '#fff',
        borderBottomWidth: 0,
        shadowColor: 'transparent',
        elevation: 0,
      },
      // headerRight 定义后，reload界面app会崩溃

      headerRight: (
        <Button
          style={{ paddingHorizontal: 12 }}
          onPress={() => params.handleSubmit && params.handleSubmit()}
        >
          <Text style={{ fontSize: 14, color: '#000' }}>{'提交'}</Text>
        </Button>
      ),
    }
  }

  submit = () => {
    const { fetching } = this.state
    if (fetching) {
      return
    }
    this.setState({ fetching: true })
    const content = this.refs.content._lastNativeText || ''
    appService
      .suggest({ content })
      .then(data => {
        this.refs.content.clear()
        this.checkError({ msg: '操作成功' })
      })
      .catch(e => {
        this.checkError(e)
      })
  }
  componentDidMount() {
    this.props.navigation.setParams({ handleSubmit: () => this.submit() })
  }
  render() {
    const { fetching } = this.state
    return (
      <RootView fetching={fetching} style={styles.container}>
        <TextInput
          ref="content"
          style={{
            paddingHorizontal: 16,
            paddingTop: 20,
            paddingBottom: 20,
            fontSize: 13,
          }}
          autoFocus
          autoGrow
          autoCapitalize="none"
          underlineColorAndroid="transparent"
          placeholder="您的意见或建议"
          placeholderTextColor="#999"
          multiline
        />
      </RootView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f9f9f9',
  },
})

export default Suggestion
