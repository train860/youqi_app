import { request } from '../utils'

export const checkToken = async () =>
  request({
    url: 'sys/user/checkToken',
    method: 'get',
    data: {},
  })

export const info = async params =>
  request({
    url: 'sys/user/info',
    method: 'get',
    data: params,
  })
